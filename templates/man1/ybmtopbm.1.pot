# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2022-08-19 19:20+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Ybmtopbm User Manual"
msgstr ""

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "06 March 1990"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr ""

#. type: UN
#: archlinux opensuse-leap-15-4
#, no-wrap
msgid "lbAB"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "ybmtopbm - convert a Bennet Yee 'face' file to PBM"
msgstr ""

#. type: UN
#: archlinux opensuse-leap-15-4
#, no-wrap
msgid "lbAC"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<ybmtopbm>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "[I<facefile>]"
msgstr ""

#. type: UN
#: archlinux opensuse-leap-15-4
#, no-wrap
msgid "lbAD"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid "This program is part of B<Netpbm>(1)"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<ymtopbm> reads a file acceptable to the B<face> and B<xbm> programs by "
"Bennet Yee (I<bsy+@cs.cmu.edu>).  and writes a PBM image as output."
msgstr ""

#. type: UN
#: archlinux opensuse-leap-15-4
#, no-wrap
msgid "lbAE"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<pbmtoybm>(1)  , B<pbm>(5)"
msgstr ""

#. type: UN
#: archlinux opensuse-leap-15-4
#, no-wrap
msgid "lbAF"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Copyright (C) 1991 by Jamie Zawinski and Jef Poskanzer."
msgstr ""

#. type: IX
#: debian-bullseye
#, no-wrap
msgid "ybmtopbm"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "ybmtopbm - convert a Bennet Yee \"face\" file into a portable bitmap"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "B<ybmtopbm> [I<facefile>]"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"Reads a file acceptable to the I<face> and I<xbm> programs by Bennet Yee "
"(bsy+@cs.cmu.edu)."
msgstr ""

#. type: IX
#: debian-bullseye
#, no-wrap
msgid "face"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "Writes a portable bitmap as output."
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid "pbmtoybm(1), pbm(5), face(1), face(5), xbm(1)"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "ybmtopbm - convert a Bennet Yee \"face\" file to PBM"
msgstr ""

#. type: UN
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "synopsis"
msgstr ""

#. type: UN
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "description"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)\\&."
msgstr ""

#. type: UN
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "options"
msgstr ""

#. type: SH
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"There are no command line options defined specifically\n"
"for B<ybmtopbm>, but it recognizes the options common to all\n"
"programs based on libnetpbm (See \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&.)\n"
msgstr ""

#. type: UN
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "seealso"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "B<pbmtoybm>(1)\\&, B<pbm>(1)\\&"
msgstr ""

#. type: UN
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "author"
msgstr ""

#. type: SH
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/ybmtopbm.html>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<pbmtoybm>(1)\\&, B<pbm>(5)\\&"
msgstr ""
