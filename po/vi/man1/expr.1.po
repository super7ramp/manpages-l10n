# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 18:46+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "EXPR"
msgstr "EXPR"

#. type: TH
#: archlinux
#, no-wrap
msgid "April 2022"
msgstr "Tháng 4 năm 2022"

#. type: TH
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "error in regular expression search"
msgid "expr - evaluate expressions"
msgstr "lỗi trong biểu thức chính quy tìm kiếm"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<expr> I<\\,EXPRESSION\\/>"
msgstr "B<expr> I<\\,BIỂU_THỨC\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<expr> I<\\,OPTION\\/>"
msgstr "B<expr> I<\\,TÙY_CHỌN\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Print the value of EXPRESSION to standard output.  A blank line below "
"separates increasing precedence groups.  EXPRESSION may be:"
msgstr ""
"In giá trị của BIỂU_THỨC ra đầu ra tiêu chuẩn.  Một dòng trắng ở dưới phân "
"cách các nhóm có quyền ưu tiên tăng dần.  BIỂU_THỨC có thể là:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 | ARG2"
msgstr "Đ.SỐ1 | Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 if it is neither null nor 0, otherwise ARG2"
msgstr "Đ.SỐ1 nếu nó không phải rỗng hoặc 0, nếu không thì Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 & ARG2"
msgstr "Đ.SỐ1 & Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 if neither argument is null or 0, otherwise 0"
msgstr "Đ.SỐ1 nếu mỗi đối số không phải rỗng hay 0, ngược lại thì 0"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<lt> ARG2"
msgstr "Đ.SỐ1 E<lt> Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is less than ARG2"
msgstr "Đ.SỐ1 nhỏ hơn Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<lt>= ARG2"
msgstr "Đ.SỐ1 E<lt>= Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is less than or equal to ARG2"
msgstr "Đ.SỐ1 nhỏ hơn hoặc bằng Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 = ARG2"
msgstr "Đ.SỐ1 = Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is equal to ARG2"
msgstr "Đ.SỐ1 bằng Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 != ARG2"
msgstr "Đ.SỐ1 != Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is unequal to ARG2"
msgstr "Đ.SỐ1 khác Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<gt>= ARG2"
msgstr "Đ.SỐ1 E<gt>= Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is greater than or equal to ARG2"
msgstr "Đ.SỐ1 lớn hơn hoặc bằng Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<gt> ARG2"
msgstr "Đ.SỐ1 E<gt> Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is greater than ARG2"
msgstr "Đ.SỐ1 lớn hơn Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 + ARG2"
msgstr "Đ.SỐ1 + Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic sum of ARG1 and ARG2"
msgstr "tổng số học của Đ.SỐ1 và Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 - ARG2"
msgstr "Đ.SỐ1 - Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic difference of ARG1 and ARG2"
msgstr "hiệu số học của Đ.SỐ1 và Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 * ARG2"
msgstr "Đ.SỐ1 * Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic product of ARG1 and ARG2"
msgstr "tích số học của Đ.SỐ1 và Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 / ARG2"
msgstr "Đ.SỐ1 / Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic quotient of ARG1 divided by ARG2"
msgstr "thương số học của Đ.SỐ1 chia cho Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 % ARG2"
msgstr "Đ.SỐ1 % Đ.SỐ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic remainder of ARG1 divided by ARG2"
msgstr "phần dư khi Đ.SỐ1 chia cho Đ.SỐ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "STRING : REGEXP"
msgstr "CHUỖI : BTCQ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "anchored pattern match of REGEXP in STRING"
msgstr "đánh dấu tương ứng của BTCQ trong CHUỖI"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "match STRING REGEXP"
msgstr "match CHUỖI BTCQ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "same as STRING : REGEXP"
msgstr "giống như CHUỖI : BTCQ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "substr STRING POS LENGTH"
msgstr "substr CHUỖI VỊ_TRÍ DÀI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "substring of STRING, POS counted from 1"
msgstr "chuỗi con của CHUỖI, VỊ_TRÍ đếm từ 1"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "index STRING CHARS"
msgstr "index CHUỖI KÝ_TỰ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "index in STRING where any CHARS is found, or 0"
msgstr "chỉ mục trong CHUỖI ở đó tìm thấy KÝ_TỰ, hoặc 0"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "length STRING"
msgstr "length CHUỖI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "length of STRING"
msgstr "chiều dài CHUỖI"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "+ TOKEN"
msgstr "+ HIỆU_BÀI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "interpret TOKEN as a string, even if it is a"
msgstr "hiểu HIỆU_BÀI như một chuỗi, thậm chí cả khi nó"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "keyword like 'match' or an operator like '/'"
msgstr "là một từ khóa như “match” hay một toán tử như “/”"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "( EXPRESSION )"
msgstr "( BIỂU_THỨC )"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "value of EXPRESSION"
msgstr "giá trị của BIỂU_THỨC"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Beware that many operators need to be escaped or quoted for shells.  "
"Comparisons are arithmetic if both ARGs are numbers, else lexicographical.  "
"Pattern matches return the string matched between \\e( and \\e) or null; if "
"\\e( and \\e) are not used, they return the number of characters matched or "
"0."
msgstr ""
"Cần biết rằng rất nhiều toán tử cần đặt sau ký tự thoát hoặc trong dấu ngoặc "
"khi gõ vào trong shell. So sánh sẽ là số học nếu cả hai Đ.SỐ đều là số, nếu "
"không sẽ là so sánh nghĩa từ. Khớp mẫu sẽ trả lại chuỗi tương ứng giữa "
"“\\e(” và “\\e)” hoặc rỗng. Nếu không dùng “\\e(” và “\\e)”, chúng sẽ trả "
"lại số ký tự tương ứng hoặc 0."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Exit status is 0 if EXPRESSION is neither null nor 0, 1 if EXPRESSION is "
"null or 0, 2 if EXPRESSION is syntactically invalid, and 3 if an error "
"occurred."
msgstr ""
"Trạng thái thoát là: 0 nếu BIỂU_THỨC không phải rỗng hoặc 0, 1 nếu BIỂU_THỨC "
"là rỗng hoặc 0, 2 nếu BIỂU_THỨC sai cú pháp, 3 nếu gặp lỗi."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by Mike Parker, James Youngman, and Paul Eggert."
msgstr "Viết bởi Mike Parker, James Youngman và Paul Eggert."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Trợ giúp trực tuyến GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Report %s translation bugs to <https://translationproject.org/team/>\n"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Hãy thông báo lỗi dịch “%s” cho <https://translationproject.org/team/vi."
"html>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/exprE<gt>"
msgstr ""
"Tài liệu đầy đủ có tại: E<lt>https://www.gnu.org/software/coreutils/exprE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) expr invocation\\(aq"
msgstr ""
"hoặc sẵn có nội bộ thông qua: info \\(aq(coreutils) expr invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Tháng 9 năm 2020"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "Tháng 10 năm 2021"

#. type: TH
#: fedora-37 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "August 2022"
msgstr "Tháng 8 năm 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "Tháng 9 năm 2021"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "GNU coreutils 9.0"

#. type: Plain text
#: mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."
