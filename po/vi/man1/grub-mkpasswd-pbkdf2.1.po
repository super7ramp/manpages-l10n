# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 18:51+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-MKPASSWD-PBKDF2"
msgstr "GRUB-MKPASSWD-PBKDF2"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2022"
msgstr "Tháng 8 năm 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r297.g0c6c1aff2-1"
msgstr ""

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-mkpasswd-pbkdf2 - generate hashed password for GRUB"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkpasswd-pbkdf2> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mkpasswd-pbkdf2> [I<\\,TÙY_CHỌN\\/>…] [I<\\,CÁC-TÙY_CHỌN\\/>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Generate PBKDF2 password hash."
msgstr "Tạo mã băm mật khẩu kiểu PBKDF2"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>, B<--iteration-count>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--iteration-count>=I<\\,SỐ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Number of PBKDF2 iterations"
msgstr "Số lần lặp của PBKDF2"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-l>, B<--buflen>=I<\\,NUM\\/>"
msgstr "B<-l>, B<--buflen>=I<\\,SỐ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Length of generated hash"
msgstr "Độ dài của mã băm tạo ra"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>, B<--salt>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--salt>=I<\\,SỐ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Length of salt"
msgstr "Độ dài của salt"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "hiển thị trợ giúp này"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "hiển thị cách sử dụng dạng ngắn gọn"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "in ra phiên bản chương trình"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Tham số là bắt buộc hay tham số chỉ là tùy chọn cho các tùy chọn dài cũng "
"đồng thời là bắt buộc hay không bắt buộc cho các tùy chọn ngắn tương ứng với "
"nó."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Hãy thông báo lỗi cho  E<lt>bug-grub@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-mkpasswd-pbkdf2> is maintained as a "
"Texinfo manual.  If the B<info> and B<grub-mkpasswd-pbkdf2> programs are "
"properly installed at your site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-mkpasswd-pbkdf2> được bảo trì dưới dạng "
"một sổ tay Texinfo.  Nếu chương trình B<info> và B<grub-mkpasswd-pbkdf2> "
"được cài đặt đúng ở địa chỉ của bạn thì câu lệnh"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-mkpasswd-pbkdf2>"
msgstr "B<info grub-mkpasswd-pbkdf2>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "July 2021"
msgstr "Tháng 7 năm 2021"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.04-20"
msgstr "GRUB 2.04-20"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "June 2020"
msgid "June 2022"
msgstr "Tháng 6 năm 2020"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "GRUB 2.06-2"
msgid "GRUB 2.06-3"
msgstr "GRUB 2.06-2"
