# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2022-08-19 19:20+0200\n"
"PO-Revision-Date: 2022-04-23 21:21+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ZMORE"
msgstr "ZMORE"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "zmore - file perusal filter for crt viewing of compressed text"
msgstr ""
"zmore - Dateiansichtsfilter für die Bildschirmausgabe von komprimiertem Text"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<zmore> [ name ...  ]"
msgstr "B<zmore> [ Name … ]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The I<zmore> command is a filter which allows examination of compressed or "
"plain text files one screenful at a time on a soft-copy terminal.  The "
"B<zmore> command works on files compressed with B<compress>, B<pack> or "
"B<gzip>, and also on uncompressed files.  If a file does not exist, B<zmore> "
"looks for a file of the same name with the addition of a .gz, .z or .Z "
"suffix."
msgstr ""
"Der Befehl B<zmore> ist ein Filter, der die seitenweise Untersuchung "
"komprimierter oder einfacher Textdateien in einem Terminal ermöglicht. Der "
"Befehl B<zmore> funktioniert mit Dateien, die mit B<compress>(1), B<pack>(1) "
"oder B<gzip>(1) komprimiert wurden, und auch mit unkomprimierten Dateien. "
"Falls eine Datei nicht existiert, sucht B<zmore> nach einer Datei mit dem "
"gleichen Namen und der Erweiterung B<.gz>, B<.z> oder B<.Z>."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<zmore> command normally pauses after each screenful, printing --More-- "
"at the bottom of the screen.  If the user then types a carriage return, one "
"more line is displayed.  If the user hits a space, another screenful is "
"displayed.  Other possibilities are enumerated later."
msgstr ""
"Der Befehl B<zmore> hält nach der Ausgabe einer Bildschirmseite an und "
"schreibt --Mehr-- an den unteren Bildrand. Drücken Sie die Eingabetaste, "
"rückt B<zmore> eine Zeile vor, bei Druck auf die Leertaste zeigt es die "
"nächste Bildschirmseite. Die anderen Möglichkeiten werden später aufgeführt."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<zmore> command looks in the file I</etc/termcap> to determine terminal "
"characteristics, and to determine the default window size.  On a terminal "
"capable of displaying 24 lines, the default window size is 22 lines.  To use "
"a pager other than the default B<more>, set environment variable PAGER to "
"the name of the desired program, such as B<less>."
msgstr ""
"Der Befehl B<zmore> ermittelt die Charakteristik und die Standardgröße des "
"Terminals aus dem Inhalt der Datei I</etc/termcap>. In einem Terminal, das "
"24 Zeilen darstellen kann, ist das Standardfenster 22 Zeilen hoch. Um einen "
"anderes Textanzeigeprogramm als den voreingestellten B<more> zu verwenden, "
"müssen Sie den Inhalt der Umgebungsvariable PAGER auf den Namen des "
"gewünschten Programms setzen, zum Beispiel B<less>."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Other sequences which may be typed when B<zmore> pauses, and their effects, "
"are as follows (I<i> is an optional integer argument, defaulting to 1) :"
msgstr ""
"Die weiteren Sequenzen, die Sie eingeben können, wenn B<zmore> pausiert, und "
"ihre Auswirkungen sind die folgenden (mit I<i> als optionalem ganzzahligem "
"Argument; Voreinstellung ist 1):"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>E<lt>spaceE<gt>"
msgstr "I<i\\^>E<lt>LeertasteE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display I<i> more lines, (or another screenful if no argument is given)"
msgstr ""
"zeigt I<i> weitere Zeilen an, oder eine weitere Bildschirmseite, falls kein "
"Argument angegeben ist."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "^D"
msgstr "^D"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"display 11 more lines (a ``scroll'').  If I<i> is given, then the scroll "
"size is set to I<i>."
msgstr ""
"zeigt 11 weitere Zeilen an (»rollt«). Ist I<i> angegeben, rollt B<zmore> "
"I<i> Zeilen weit."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "d"
msgstr "d"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "same as ^D (control-D)"
msgstr "ist gleichbedeutend mit ^D (Strg-D)"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>z"
msgstr "I<i\\^>z"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"same as typing a space except that I<i>, if present, becomes the new window "
"size."
msgstr ""
"ist gleichbedeutend mit dem Drücken der Leertaste, außer dass bei "
"vorhandenem I<i> dieses zur neuen Fenstergröße wird."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>s"
msgstr "I<i\\^>s"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "skip I<i> lines and print a screenful of lines"
msgstr "überspringt I<i> Zeilen und gibt eine neue Bildschirmseite aus."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>f"
msgstr "I<i\\^>f"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "skip I<i> screenfuls and print a screenful of lines"
msgstr "überspringt I<i> Bildschirmseiten und gibt eine neue aus."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "q or Q"
msgstr "q oder Q"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Quit."
msgstr "Beenden."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "="
msgstr "="

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display the current line number."
msgstr "zeigt die aktuelle Zeilennummer an."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i>/expr"
msgstr "I<i>/Ausdruck"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"search for the I<i\\^>-th occurrence of the regular expression I<expr.> The "
"user's erase and kill characters may be used to edit the regular "
"expression.  Erasing back past the first column cancels the search command."
msgstr ""
"sucht nach dem I<i\\^>-ten Auftreten des regulären Ausdrucks I<Ausdruck>. "
"Die Lösch- und Kill-Zeichen können zum Bearbeiten des regulären Ausdrucks "
"verwendet werden. Das vollständige Löschen auch des ersten Zeichens bricht "
"den Suchbefehl ab."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>n"
msgstr "I<i\\^>n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"search for the I<i\\^>-th occurrence of the last regular expression entered."
msgstr ""
"sucht nach dem I<i\\^>-ten Auftreten des zuletzt eingegebenen regulären "
"Ausdrucks."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "!command"
msgstr "!Befehl"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"invoke a shell with I<command>.  The character `!' in \"command\" is "
"replaced with the previous shell command.  The sequence \"\\e!\" is replaced "
"by \"!\"."
msgstr ""
"öffnet eine Shell mit dem I<Befehl>. Das Zeichen »!« im »Befehl« wird durch "
"den vorherigen Shell-Befehl ersetzt. Die Sequenz »\\e!« wird durch »!« "
"ersetzt."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ":q or :Q"
msgstr ":q oder :Q"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Quit (same as q or Q)."
msgstr "Beenden (gleichbedeutend mit q oder Q)."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "."
msgstr "."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "(dot) repeat the previous command."
msgstr "(Punkt) wiederholt den vorigen Befehl."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The commands take effect immediately, i.e., it is not necessary to type a "
"carriage return.  Up to the time when the command character itself is given, "
"the user may hit the line kill character to cancel the numerical argument "
"being formed.  In addition, the user may hit the erase character to "
"redisplay the --More-- message."
msgstr ""
"Der Befehl wird unmittelbar ausgeführt, das bedeutet, ein Drücken der "
"Eingabetaste ist nicht nötig. Bis das Befehlszeichen selbst eingegeben wird, "
"können Sie das Zeichen zum Löschen der Zeile eingeben, um das numerische "
"Argument abzubrechen. Zusätzlich kann das Löschzeichen eingegeben werden, um "
"die Meldung --Mehr-- wieder anzuzeigen."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"At any time when output is being sent to the terminal, the user can hit the "
"quit key (normally control-\\e).  The I<zmore> command will stop sending "
"output, and will display the usual --More-- prompt.  The user may then enter "
"one of the above commands in the normal manner.  Unfortunately, some output "
"is lost when this is done, due to the fact that any characters waiting in "
"the terminal's output queue are flushed when the quit signal occurs."
msgstr ""
"Solange die Ausgabe an das Terminal gesendet wird, können Sie jederzeit die "
"Beenden-Taste drücken, normalerweise Strg-\\e). Der Befehl I<zmore> "
"unterbricht dann das Senden an die Ausgabe und zeigt die Zeile --Mehr-- an. "
"Sie können dann einen der oben genannten Befehle in der üblichen Weise "
"eingeben. Allerdings geht dabei ein Teil der Ausgabe verloren, weil einige "
"Zeichen in der Ausgabewarteschlange gelöscht werden, wenn das Beenden-Signal "
"abgesetzt wird."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The terminal is set to I<noecho> mode by this program so that the output can "
"be continuous.  What you type will thus not show on your terminal, except "
"for the / and ! commands."
msgstr ""
"Das Terminal wird durch dieses Programm in den I<noecho>-Modus versetzt, so "
"dass die Anzeige fortlaufend erfolgen kann. Ihre Eingaben werden daher nicht "
"in diesem Terminal angezeigt, außer die Befehle »/« und »!«."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If the standard output is not a teletype, then B<zmore> acts just like "
"B<zcat>, except that a header is printed before each file if there is more "
"than one file."
msgstr ""
"Falls die Standardausgabe kein Terminal ist, dann arbeitet B<zmore> wie "
"B<zcat>(1), außer dass die Kopfzeile vor jeder Datei ausgegeben wird."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

# FIXME /etc/termcap → I</etc/termcap>
#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "/etc/termcap"
msgstr "I</etc/termcap>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Terminal data base"
msgstr "Terminal-Datenbank"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<more>(1), B<gzip>(1), B<zdiff>(1), B<zgrep>(1), B<znew>(1), B<zforce>(1), "
"B<gzexe>(1)"
msgstr ""
"B<more>(1), B<gzip>(1), B<zdiff>(1), B<zgrep>(1), B<znew>(1), B<zforce>(1), "
"B<gzexe>(1)"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"I<Zmore> is a filter which allows examination of compressed or plain text "
"files one screenful at a time on a soft-copy terminal.  I<zmore> works on "
"files compressed with I<compress>, I<pack> or I<gzip>, and also on "
"uncompressed files.  If a file does not exist, I<zmore> looks for a file of "
"the same name with the addition of a .gz, .z or .Z suffix."
msgstr ""
"B<Zmore> ist ein Filter, der die seitenweise Untersuchung komprimierter oder "
"einfacher Textdateien in einem Terminal ermöglicht. B<Zmore> funktioniert "
"mit Dateien, die mit B<compress>(1), B<pack>(1) oder B<gzip>()1 komprimiert "
"wurden, und auch mit unkomprimierten Dateien. Falls eine Datei nicht "
"existiert, sucht B<zmore> nach einer Datei mit dem gleichen Namen und der "
"Erweiterung B<.gz>, B<.z> oder B<.Z>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"I<Zmore> normally pauses after each screenful, printing --More-- at the "
"bottom of the screen.  If the user then types a carriage return, one more "
"line is displayed.  If the user hits a space, another screenful is "
"displayed.  Other possibilities are enumerated later."
msgstr ""
"B<Zmore> hält nach der Ausgabe einer Bildschirmseite an und schreibt --"
"Mehr-- an den unteren Bildrand. Drücken Sie die Eingabetaste, rückt B<zmore> "
"eine Zeile vor, bei Druck auf die Leertaste zeigt es die nächste "
"Bildschirmseite. Die anderen Möglichkeiten werden später aufgeführt."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"I<Zmore> looks in the file I</etc/termcap> to determine terminal "
"characteristics, and to determine the default window size.  On a terminal "
"capable of displaying 24 lines, the default window size is 22 lines.  To use "
"a pager other than the default I<more>, set environment variable PAGER to "
"the name of the desired program, such as I<less>."
msgstr ""
"B<Zmore> ermittelt die Charakteristik und die Standardgröße des Terminals "
"aus dem Inhalt der Datei I</etc/termcap>. In einem Terminal, das 24 Zeilen "
"darstellen kann, ist das Standardfenster 22 Zeilen hoch. Um einen anderes "
"Textanzeigeprogramm als den voreingestellten B<more>(1) zu verwenden, müssen "
"Sie den Inhalt der Umgebungsvariable PAGER auf den Namen des gewünschten "
"Programms setzen, zum Beispiel B<less>(1)."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"Other sequences which may be typed when I<zmore> pauses, and their effects, "
"are as follows (I<i> is an optional integer argument, defaulting to 1) :"
msgstr ""
"Die weiteren Sequenzen, die Sie eingeben können, wenn B<zmore> pausiert, und "
"ihre Auswirkungen sind die folgenden (mit I<i> als optionalem ganzzahligem "
"Argument; Voreinstellung ist 1):"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"At any time when output is being sent to the terminal, the user can hit the "
"quit key (normally control-\\e).  I<Zmore> will stop sending output, and "
"will display the usual --More-- prompt.  The user may then enter one of the "
"above commands in the normal manner.  Unfortunately, some output is lost "
"when this is done, due to the fact that any characters waiting in the "
"terminal's output queue are flushed when the quit signal occurs."
msgstr ""
"Solange die Ausgabe an das Terminal gesendet wird, können Sie jederzeit die "
"Beenden-Taste drücken, normalerweise Strg-\\e). I<Zmore> unterbricht dann "
"das Senden an die Ausgabe und zeigt die Zeile --Mehr-- an. Sie können dann "
"einen der oben genannten Befehle in der üblichen Weise eingeben. Allerdings "
"geht dabei ein Teil der Ausgabe verloren, weil einige Zeichen in der "
"Ausgabewarteschlange gelöscht werden, wenn das Beenden-Signal abgesetzt wird."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"If the standard output is not a teletype, then I<zmore> acts just like "
"I<zcat>, except that a header is printed before each file if there is more "
"than one file."
msgstr ""
"Falls die Standardausgabe kein Terminal ist, dann arbeitet B<zmore> wie "
"B<zcat>(1), außer dass die Kopfzeile vor jeder Datei ausgegeben wird."

# FIXME missing markup
#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "more(1), gzip(1), zdiff(1), zgrep(1), znew(1), zforce(1), gzexe(1)"
msgstr ""
"B<more>(1), B<gzip>(1), B<zdiff>(1), B<zgrep>(1), B<znew>(1), B<zforce>(1), "
"B<gzexe>(1)"
