# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 19:07+0200\n"
"PO-Revision-Date: 2022-02-10 23:20+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SCRIPTLIVE"
msgstr "SCRIPTLIVE"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "scriptlive - re-run session typescripts, using timing information"
msgstr ""
"scriptlive - Sitzungs-Eingabeskripte unter Verwendung von Timing-"
"Informationen wiedergeben"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<scriptlive> [options] [B<-t>] I<timingfile> [B<-I>|B<-B>] I<typescript>"
msgstr ""
"B<scriptlive> [Optionen] [B<-t>] I<Timing-Datei> [B<-I>|B<-B>] "
"I<Eingabeskript>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This program re-runs a typescript, using stdin typescript and timing "
"information to ensure that input happens in the same rhythm as it originally "
"appeared when the script was recorded."
msgstr ""
"Dieses Programm gibt ein Eingabeskript aus der Standardeingabe wieder und "
"stellt über die Timing-Informationen sicher, dass die Ausgabe im gleichen "
"Rhythmus wie bei der ursprünglichen Aufzeichnung des Skripts erfolgt."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<session is executed> in a newly created pseudoterminal with the "
"user\\(cqs $SHELL (or defaults to I</bin/bash>)."
msgstr ""
"Die B<Sitzung wird mit der $SHELL des Benutzers in neu erstellten Pseudo-"
"Terminals ausgeführt> (oder standardmäßig I</bin/bash>)."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<Be careful!> Do not forget that the typescript may contains arbitrary "
"commands. It is recommended to use B<\"scriptreplay --stream in --log-in "
"typescript\"> (or with B<--log-io> instead of B<--log-in>) to verify the "
"typescript before it is executed by B<scriptlive>."
msgstr ""
"B<Vorsicht!> Vergessen Sie nicht, dass das Eingabeskript alle möglichen "
"Befehle enthalten kann. Es ist empfehlenswert, »B<scriptreplay --stream in --"
"log-in Eingabeskript>« (oder mit B<--log-io> anstelle von B<--log-in>) zu "
"verwenden, um das Eingabeskript vor der Ausführung durch B<scriptlive> zu "
"verifizieren."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"The timing information is what B<script>(1) outputs to file specified by B<--"
"log-timing>. The typescript has to contain stdin information and it is what "
"script1 outputs to file specified by B<--log-in> or B<--log-io>."
msgstr ""
"Die Timing-Information ist jene, die B<script>(1) in die mit B<--log-timing> "
"angegebene Datei schreibt. Das Eingabeskript muss die Informationen der "
"Standardeingabe enthalten und ist das, was B<script>(1) in die mit B<--log-"
"in> oder B<--log-io> angegebene Datei schreibt."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-I>, B<--log-in> I<file>"
msgstr "B<-I>, B<--log-in> I<Datei>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "File containing B<script>\\(aqs terminal input."
msgstr "gibt die Datei an, welche die Terminaleingaben von B<script> enthält."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-B>, B<--log-io> I<file>"
msgstr "B<-B>, B<--log-io> I<Datei>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "File containing B<script>\\(aqs terminal output and input."
msgstr ""
"gibt die  Datei an, welche die Terminalaus- und -eingaben von B<script> "
"enthält."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-t>, B<--timing> I<file>"
msgstr "B<-t>, B<--timing> I<Datei>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"File containing B<script>\\(aqs timing output. This option overrides old-"
"style arguments."
msgstr ""
"gibt die Datei an, welche die Timing-Informationen von B<script> enthält. "
"Diese Option setzt die Argumente im alten Stil außer Kraft."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-T>, B<--log-timing> I<file>"
msgstr "B<-T>, B<--log-timing> I<Datei>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Aliased to B<-t>, maintained for compatibility with B<script>(1) command-"
"line options."
msgstr ""
"ist ein Alias für B<-t>, der zwecks Kompatibilität zu den "
"Befehlszeilenoptionen von B<script>(1) erhalten wird."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-d>, B<--divisor> I<number>"
msgstr "B<-d>, B<--divisor> I<Faktor>"

# NOTE Der vorletzte Satz wäre im Deutschen Nonsens.
#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Speed up the replay displaying this I<number> of times. The argument is a "
"floating-point number. It\\(cqs called divisor because it divides the "
"timings by this factor. This option overrides old-style arguments."
msgstr ""
"skaliert die Wiedergabegeschwindigkeit um den angegebenen I<Faktor>. Das "
"Argument ist eine Gleitkommazahl. Diese Option setzt Argumente im "
"klassischen Stil außer Kraft."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-m>, B<--maxdelay> I<number>"
msgstr "B<-m>, B<--maxdelay> I<Anzahl>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Set the maximum delay between updates to I<number> of seconds. The argument "
"is a floating-point number. This can be used to avoid long pauses in the "
"typescript replay."
msgstr ""
"gibt die maximale Verzögerung zwischen den Aktualisierungen des Skripts als "
"I<Anzahl> Sekunden an. Das Argument ist eine Gleitkommazahl. Damit können "
"Sie lange Pausen in der Wiedergabe des Eingabeskripts vermeiden."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"% script --log-timing file.tm --log-in script.in\n"
"Script started, file is script.out\n"
"% date\n"
"E<lt>etc, etcE<gt>\n"
"% exit\n"
"Script done, file is script.out\n"
"% scriptlive --log-timing file.tm --log-in script.in\n"
msgstr ""
"% script --log-timing file.tm --log-in script.in\n"
"Skript gestartet, Datei ist script.out\n"
"% date\n"
"E<lt>usw.E<gt>\n"
"% exit\n"
"Skript wurde beendet, Datei ist script.out\n"
"% scriptlive --log-timing file.tm --log-in script.in\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Copyright © 2019 Karel Zak"
msgstr "Copyright © 2019 Karel Zak"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software; see the source for copying conditions. There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Dies ist freie Software, in den Quellen finden Sie die Bedingungen zur "
"Weitergabe. Es gibt KEINE Garantie, auch nicht für die MARKTREIFE oder die "
"TAUGLICHKEIT FÜR EINEN BESTIMMTEN ZWECK."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Released under the GNU General Public License version 2 or later."
msgstr ""
"Veröffentlicht unter den Bedingungen der GNU General Public License Version "
"2 oder neuer."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<script>(1), B<scriptreplay>(1)"
msgstr "B<script>(1), B<scriptreplay>(1)"

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<scriptlive> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<scriptlive> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "October 2019"
msgstr "Oktober 2019"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid "B<scriptlive> [options] [B<-t>] I<timingfile> [B<-I|-B>] I<typescript>"
msgstr ""
"B<scriptlive> [Optionen] [B<-t>] I<Timing-Datei> [B<-I|-B>] I<Eingabeskript>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The B<session is executed> in a newly created pseudoterminal with the user's "
"$SHELL (or defaults to /bin/bash)."
msgstr ""
"Die B<Sitzung wird mit der $SHELL des Benutzers in neu erstellten Pseudo-"
"Terminals ausgeführt> (oder standardmäßig /bin/bash)."

#. type: Plain text
#: debian-bullseye
msgid ""
"B<Be careful!> Do not forget that the typescript may contains arbitrary "
"commands.  It is recommended to use B<\"scriptreplay --stream in --log-in "
"typescript\"> (or with B<--log-io> instead of B<--log-in\\)> to verify the "
"typescript before it is executed by B<scriptlive>(1)."
msgstr ""
"B<Vorsicht!> Vergessen Sie nicht, dass das Eingabeskript alle möglichen "
"Befehle enthalten kann. Es ist empfehlenswert, »B<scriptreplay --stream in --"
"log-in Eingabeskript>« (oder mit B<--log-io> anstelle von B<--log-in\\>) zu "
"verwenden, um das Eingabeskript vor der Ausführung durch B<scriptlive>(1) zu "
"verifizieren."

#. type: Plain text
#: debian-bullseye
msgid ""
"The timing information is what B<script>(1)  outputs to file specified by "
"B<--log-timing>.  The typescript has to contain stdin information and it is "
"what B<script>(1)  outputs to file specified by B<--log-in> or B<--log-io>."
msgstr ""
"Die Timing-Information ist jene, die B<script>(1) in die mit B<--log-timing> "
"angegebene Datei schreibt. Das Eingabeskript muss die Informationen der "
"Standardeingabe enthalten und ist das, was B<script>(1) in die mit B<--log-"
"in> oder B<--log-io> angegebene Datei schreibt."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-I>,B< --log-in >I<file>"
msgstr "B<-I>,B< --log-in >I<Datei>"

#. type: Plain text
#: debian-bullseye
msgid "File containing B<script>'s terminal input."
msgstr "gibt die Datei an, welche die Terminaleingaben von B<script> enthält."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-B>,B< --log-io >I<file>"
msgstr "B<-B>,B< --log-io >I<Datei>"

#. type: Plain text
#: debian-bullseye
msgid "File containing B<script>'s terminal output and input."
msgstr ""
"gibt die  Datei an, welche die Terminalaus- und -eingaben von B<script> "
"enthält."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-t>,B< --timing >I<file>"
msgstr "B<-t>,B< --timing >I<Datei>"

#. type: Plain text
#: debian-bullseye
msgid ""
"File containing B<script>'s timing output.  This option overrides old-style "
"arguments."
msgstr ""
"gibt die Datei an, welche die Timing-Informationen von B<script> enthält. "
"Diese Option setzt die Argumente im alten Stil außer Kraft."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-T>,B< --log-timing >I<file>"
msgstr "B<-T>,B< --log-timing >I<Datei>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Aliased to B<-t>, maintained for compatibility with B<script>(1)  command-"
"line options."
msgstr ""
"ist ein Alias für B<-t>, der zwecks Kompatibilität zu den "
"Befehlszeilenoptionen von B<script>(1) erhalten wird."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-d>,B< --divisor >I<number>"
msgstr "B<-d>,B< --divisor >I<Faktor>"

# NOTE Der vorletzte Satz wäre im Deutschen Nonsens.
#. type: Plain text
#: debian-bullseye
msgid ""
"Speed up the replay displaying this I<number> of times.  The argument is a "
"floating-point number.  It's called divisor because it divides the timings "
"by this factor.  This option overrides old-style arguments."
msgstr ""
"skaliert die Wiedergabegeschwindigkeit um den angegebenen I<Faktor>. Das "
"Argument ist eine Gleitkommazahl. Diese Option setzt Argumente im "
"klassischen Stil außer Kraft."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-m>,B< --maxdelay >I<number>"
msgstr "B<-m>,B< --maxdelay >I<Anzahl>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Set the maximum delay between updates to I<number> of seconds.  The argument "
"is a floating-point number.  This can be used to avoid long pauses in the "
"typescript replay."
msgstr ""
"gibt die maximale Verzögerung zwischen den Aktualisierungen des Skripts als "
"I<Anzahl> Sekunden an. Das Argument ist eine Gleitkommazahl. Damit können "
"Sie lange Pausen in der Wiedergabe des Eingabeskripts vermeiden."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: debian-bullseye
msgid "E<.MT kzak@\\:redhat.com> Karel Zak E<.ME .>"
msgstr "E<.MT kzak@\\:redhat.com> Karel Zak E<.ME .>"

#. type: Plain text
#: debian-bullseye
msgid "Copyright \\(co 2019 Karel Zak"
msgstr "Copyright \\(co 2019 Karel Zak"

#. type: Plain text
#: debian-bullseye
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Dies ist freie Software, in den Quellen finden Sie die Bedingungen zur "
"Weitergabe. Es gibt KEINE Garantie, auch nicht für die MARKTREIFE oder die "
"TAUGLICHKEIT FÜR EINEN BESTIMMTEN ZWECK."

# FIXME scriptlive → B<scriptlive>
#. type: Plain text
#: debian-bullseye
msgid ""
"The scriptlive command is part of the util-linux package and is available "
"from E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/"
"> Linux Kernel Archive E<.UE .>"
msgstr ""
"Der Befehl B<scriptlive> ist Teil des Pakets util-linux, welches aus dem E<."
"UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel-Archiv E<.UE > heruntergeladen werden kann."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2021-06-02"
msgstr "2. Juni 2021"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The timing information is what script1 outputs to file specified by B<--log-"
"timing>. The typescript has to contain stdin information and it is what "
"script1 outputs to file specified by B<--log-in> or B<--log-io>."
msgstr ""
"Die Timing-Information ist jene, die B<script>(1) in die mit B<--log-timing> "
"angegebene Datei schreibt. Das Eingabeskript muss die Informationen der "
"Standardeingabe enthalten und ist das, was B<script>(1) in die mit B<--log-"
"in> oder B<--log-io> angegebene Datei schreibt."

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
