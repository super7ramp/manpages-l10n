# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2022-08-19 18:51+0200\n"
"PO-Revision-Date: 2021-03-07 13:01+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-MKPASSWD-PBKDF2"
msgstr "GRUB-MKPASSWD-PBKDF2"

#. type: TH
#: archlinux
#, no-wrap
msgid "August 2022"
msgstr "August 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r297.g0c6c1aff2-1"
msgstr "GRUB 2:2.06.r297.g0c6c1aff2-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-mkpasswd-pbkdf2 - generate hashed password for GRUB"
msgstr "grub-mkpasswd-pbkdf2 - einen Passwort-Hash für GRUB erzeugen"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkpasswd-pbkdf2> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mkpasswd-pbkdf2> [I<\\,OPTION\\/>…] [I<\\,OPTIONEN\\/>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Generate PBKDF2 password hash."
msgstr "Erzeugt einen PBKDF2-Passwort-Hash."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>, B<--iteration-count>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--iteration-count>=I<\\,ANZAHL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Number of PBKDF2 iterations"
msgstr "legt die Anzahl der PBKDF2-Durchläufe fest."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-l>, B<--buflen>=I<\\,NUM\\/>"
msgstr "B<-l>, B<--buflen>=I<\\,ZAHL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Length of generated hash"
msgstr "gibt die Länge des erzeugten Hashs an."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>, B<--salt>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--salt>=I<\\,ZAHL\\/>"

# https://de.wikipedia.org/wiki/Salt_(Kryptologie)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Length of salt"
msgstr "gibt die Salt-Länge (angehängte Zufallszeichenfolge) an."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Erforderliche oder optionale Argumente für lange Optionen sind ebenso "
"erforderlich bzw. optional für die entsprechenden Kurzoptionen."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-mkpasswd-pbkdf2> is maintained as a "
"Texinfo manual.  If the B<info> and B<grub-mkpasswd-pbkdf2> programs are "
"properly installed at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-mkpasswd-pbkdf2> wird als ein "
"Texinfo-Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-mkpasswd-"
"pbkdf2> auf Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem "
"Befehl"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-mkpasswd-pbkdf2>"
msgstr "B<info grub-mkpasswd-pbkdf2>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "July 2021"
msgstr "Juli 2021"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.04-20"
msgstr "GRUB 2.04-20"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "June 2022"
msgstr "Juni 2022"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-3"
msgstr "GRUB 2.06-3"
