# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014-2015, 2020, 2021.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2022-08-19 18:53+0200\n"
"PO-Revision-Date: 2021-05-22 10:22+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "IPCRM"
msgstr "IPCRM"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ipcrm - remove certain IPC resources"
msgstr "ipcrm - bestimmte IPC-Ressourcen entfernen"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<ipcrm> [options]"
msgstr "B<ipcrm> [Optionen]"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<ipcrm> [B<shm>|B<msg>|B<sem>] I<ID> ..."
msgstr "B<ipcrm> [B<shm>|B<msg>|B<sem>] I<ID> …"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<ipcrm> removes System V inter-process communication (IPC) objects and "
"associated data structures from the system. In order to delete such objects, "
"you must be superuser, or the creator or owner of the object."
msgstr ""
"I<ipcrm> entfernt Objekte der System V Interprocess Communication (IPC) und "
"die zugehörigen Datenstrukturen aus dem System. Um solche Objekte entfernen "
"zu können, müssen Sie entweder Administrator oder der Ersteller oder "
"Besitzer des Objektes sein."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"System V IPC objects are of three types: shared memory, message queues, and "
"semaphores. Deletion of a message queue or semaphore object is immediate "
"(regardless of whether any process still holds an IPC identifier for the "
"object). A shared memory object is only removed after all currently attached "
"processes have detached (B<shmdt>(2)) the object from their virtual address "
"space."
msgstr ""
"IPC-Objekte von System V gibt es in drei Typen: gemeinsam genutzter "
"Speicher, Nachrichtenwarteschlangen und Semaphoren. "
"Nachrichtenwarteschlangen oder Semaphor-Objekte werden unmittelbar gelöscht, "
"unabhängig davon, ob ein Prozess einen IPC-Bezeichner für dieses Objekt "
"besitzt. Ein gemeinsam genutztes Speicherobjekt wird nur dann gelöscht, wenn "
"alle aktuell angehängten Prozesse das Objekt von deren virtuellem Adressraum "
"abgehängt haben (B<shmdt>(2))."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Two syntax styles are supported. The old Linux historical syntax specifies a "
"three-letter keyword indicating which class of object is to be deleted, "
"followed by one or more IPC identifiers for objects of this type."
msgstr ""
"Zwei Syntax-Stile werden unterstützt. Die historische Linux-Syntax verwendet "
"ein Schlüsselwort aus drei Buchstaben, welches die Klasse des zu löschenden "
"Objekts angibt, gefolgt von einem oder mehreren IPC-Bezeichnern für Objekte "
"dieses Typs."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The SUS-compliant syntax allows the specification of zero or more objects of "
"all three types in a single command line, with objects specified either by "
"key or by identifier (see below). Both keys and identifiers may be specified "
"in decimal, hexadecimal (specified with an initial \\(aq0x\\(aq or "
"\\(aq0X\\(aq), or octal (specified with an initial \\(aq0\\(aq)."
msgstr ""
"Die SUS-konforme Syntax ermöglicht die Angabe von null oder mehr Objekten "
"aller drei Typen in einer einzigen Befehlszeile, wobei Objekte entweder "
"durch Schlüssel oder Bezeichner angegeben werden, siehe unten. Sowohl "
"Schlüssel als auch Bezeichner können in dezimaler, hexadezimaler (angegeben "
"durch ein initiales »0x« oder »0X«) oder oktaler (angegeben durch eine "
"initiale »0«) Schreibweise übergeben werden."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The details of the removes are described in B<shmctl>(2), B<msgctl>(2), and "
"B<semctl>(2). The identifiers and keys can be found by using B<ipcs>(1)."
msgstr ""
"Details zu den Löschvorgängen werden in I<msgctl>(2), I<shmctl>(2) und "
"I<semctl>(2) beschrieben. Die Bezeichner und Schlüssel können durch "
"I<ipcs>(1) ermittelt werden."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. #-#-#-#-#  archlinux: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-37: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: ipcrm.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all> [B<shm>] [B<msg>] [B<sem>]"
msgstr "B<-a>, B<--all> [B<shm>] [B<msg>] [B<sem>]"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Remove all resources. When an option argument is provided, the removal is "
"performed only for the specified resource types."
msgstr ""
"entfernt alle Ressourcen. Wenn ein Optionsargument angegeben wird, dann "
"werden nur die Ressourcen der angegebenen Typen entfernt."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"I<Warning!> Do not use B<-a> if you are unsure how the software using the "
"resources might react to missing objects. Some programs create these "
"resources at startup and may not have any code to deal with an unexpected "
"disappearance."
msgstr ""
"I<Achtung!> Verwenden Sie B<-a> nicht, wenn Sie nicht sicher sind, wie die "
"Software darauf reagiert, welche die Ressourcen verwendet. Einige Programme "
"erzeugen diese Ressourcen beim Start und können mit deren unerwartetem "
"Verschwinden nicht umgehen."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-M>, B<--shmem-key> I<shmkey>"
msgstr "B<-M>, B<--shmem-key> I<shmkey>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Remove the shared memory segment created with I<shmkey> after the last "
"detach is performed."
msgstr ""
"entfernt das mit I<shmkey> erzeugte gemeinsam genutzte Speichersegment, "
"nachdem der letzte Abhängevorgang abgeschlossen wurde."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-m>, B<--shmem-id> I<shmid>"
msgstr "B<-m>, B<--shmem-id> I<shmid>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Remove the shared memory segment identified by I<shmid> after the last "
"detach is performed."
msgstr ""
"entfernt das mit I<shmid> angegebene gemeinsam genutzte Speichersegment, "
"nachdem der letzte Abhängevorgang abgeschlossen wurde."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-Q>, B<--queue-key> I<msgkey>"
msgstr "B<-Q>, B<--queue-key> I<msgkey>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Remove the message queue created with I<msgkey>."
msgstr "entfernt die mit I<msgkey> erzeugte Nachrichtenwarteschlange."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-q>, B<--queue-id> I<msgid>"
msgstr "B<-q>, B<--queue-id> I<msgid>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Remove the message queue identified by I<msgid>."
msgstr "entfernt die mit I<msgid> angegebene Nachrichtenwarteschlange."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-S>, B<--semaphore-key> I<semkey>"
msgstr "B<-S>, B<--semaphore-key> I<semkey>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Remove the semaphore created with I<semkey>."
msgstr "entfernt den mit I<semkey> erzeugten Semaphor."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-s>, B<--semaphore-id> I<semid>"
msgstr "B<-s>, B<--semaphore-id> I<semid>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Remove the semaphore identified by I<semid>."
msgstr "entfernt den mit I<semid> angegebenen Semaphor."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"In its first Linux implementation, B<ipcrm> used the deprecated syntax shown "
"in the second line of the B<SYNOPSIS>. Functionality present in other *nix "
"implementations of B<ipcrm> has since been added, namely the ability to "
"delete resources by key (not just identifier), and to respect the same "
"command-line syntax. For backward compatibility the previous syntax is still "
"supported."
msgstr ""
"In der ersten Linux-Implementation von B<ipcrm> wurde die in der zweiten "
"Zeile der B<ÜBERSICHT> angezeigte veraltete Syntax verwendet. Seither wurden "
"die in anderen *nix-Implementationen von B<ipcrm> verfügbaren Funktionen "
"hinzugefügt, insbesondere die Möglichkeit zum Löschen von Ressourcen anhand "
"des Schlüssels (nicht nur anhand des Bezeichners) und die Verwendung der "
"gleichen Befehlszeilensyntax. Aus Gründen der Abwärtskompatibilität wird die "
"alte Syntax weiterhin unterstützt."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<ipcmk>(1), B<ipcs>(1), B<msgctl>(2), B<msgget>(2), B<semctl>(2), "
"B<semget>(2), B<shmctl>(2), B<shmdt>(2), B<shmget>(2), B<ftok>(3), "
"B<sysvipc>(7)"
msgstr ""
"B<ipcmk>(1), B<ipcs>(1), B<msgctl>(2), B<msgget>(2), B<semctl>(2), "
"B<semget>(2), B<shmctl>(2), B<shmdt>(2), B<shmget>(2), B<ftok>(3), "
"B<sysvipc>(7)"

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<ipcrm> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<ipcrm> ist Teil des Pakets util-linux, welches heruntergeladen "
"werden kann von:"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "July 2014"
msgstr "Juli 2014"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid "B<ipcrm> {B<shm>|B<msg>|B<sem>} I<id>..."
msgstr "B<ipcrm> {B<shm>|B<msg>|B<sem>} I<ID> …"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<ipcrm> removes System V inter-process communication (IPC) objects and "
"associated data structures from the system.  In order to delete such "
"objects, you must be superuser, or the creator or owner of the object."
msgstr ""
"I<ipcrm> entfernt Objekte der System V Interprocess Communication (IPC) und "
"die zugehörigen Datenstrukturen aus dem System. Um solche Objekte entfernen "
"zu können, müssen Sie entweder Administrator oder der Ersteller oder "
"Besitzer des Objektes sein."

#. type: Plain text
#: debian-bullseye
msgid ""
"System V IPC objects are of three types: shared memory, message queues, and "
"semaphores.  Deletion of a message queue or semaphore object is immediate "
"(regardless of whether any process still holds an IPC identifier for the "
"object).  A shared memory object is only removed after all currently "
"attached processes have detached (B<shmdt>(2))  the object from their "
"virtual address space."
msgstr ""
"IPC-Objekte von System V gibt es in drei Typen: gemeinsam genutzter "
"Speicher, Nachrichtenwarteschlangen und Semaphoren. "
"Nachrichtenwarteschlangen oder Semaphor-Objekte werden unmittelbar gelöscht, "
"unabhängig davon, ob ein Prozess einen IPC-Bezeichner für dieses Objekt "
"besitzt. Ein gemeinsam genutztes Speicherobjekt wird nur dann gelöscht, wenn "
"alle aktuell angehängten Prozesse das Objekt von deren virtuellem Adressraum "
"abgehängt haben (B<shmdt>(2))."

#. type: Plain text
#: debian-bullseye
msgid ""
"Two syntax styles are supported.  The old Linux historical syntax specifies "
"a three-letter keyword indicating which class of object is to be deleted, "
"followed by one or more IPC identifiers for objects of this type."
msgstr ""
"Zwei Syntax-Stile werden unterstützt. Die historische Linux-Syntax verwendet "
"ein Schlüsselwort aus drei Buchstaben, welches die Klasse des zu löschenden "
"Objekts angibt, gefolgt von einem oder mehreren IPC-Bezeichnern für Objekte "
"dieses Typs."

#. type: Plain text
#: debian-bullseye
msgid ""
"The SUS-compliant syntax allows the specification of zero or more objects of "
"all three types in a single command line, with objects specified either by "
"key or by identifier (see below).  Both keys and identifiers may be "
"specified in decimal, hexadecimal (specified with an initial '0x' or '0X'), "
"or octal (specified with an initial '0')."
msgstr ""
"Die SUS-konforme Syntax ermöglicht die Angabe von null oder mehr Objekten "
"aller drei Typen in einer einzigen Befehlszeile, wobei Objekte entweder "
"durch Schlüssel oder Bezeichner angegeben werden, siehe unten. Sowohl "
"Schlüssel als auch Bezeichner können in dezimaler, hexadezimaler (angegeben "
"durch ein initiales »0x« oder »0X«) oder oktaler (angegeben durch eine "
"initiale »0«) Schreibweise übergeben werden."

#. type: Plain text
#: debian-bullseye
msgid ""
"The details of the removes are described in B<shmctl>(2), B<msgctl>(2), and "
"B<semctl>(2).  The identifiers and keys can be found by using B<ipcs>(1)."
msgstr ""
"Details zu den Löschvorgängen werden in I<msgctl>(2), I<shmctl>(2) und "
"I<semctl>(2) beschrieben. Die Bezeichner und Schlüssel können durch "
"I<ipcs>(1) ermittelt werden."

#. type: Plain text
#: debian-bullseye
msgid ""
"Remove all resources.  When an option argument is provided, the removal is "
"performed only for the specified resource types.  I<Warning!> Do not use B<-"
"a> if you are unsure how the software using the resources might react to "
"missing objects.  Some programs create these resources at startup and may "
"not have any code to deal with an unexpected disappearance."
msgstr ""
"entfernt alle Ressourcen. Wenn ein Optionsargument angegeben wird, dann "
"werden nur die Ressourcen der angegebenen Typen entfernt. I<Achtung!> "
"Verwenden Sie B<-a> nicht, wenn Sie nicht sicher sind, wie die Software "
"darauf reagiert, welche die Ressourcen verwendet. Einige Programme erzeugen "
"diese Ressourcen beim Start und können mit deren unerwartetem Verschwinden "
"nicht umgehen."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-M>,B< --shmem-key >I<shmkey>"
msgstr "B<-M>,B< --shmem-key >I<shmkey>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-m>,B< --shmem-id >I<shmid>"
msgstr "B<-m>,B< --shmem-id >I<shmid>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-Q>,B< --queue-key >I<msgkey>"
msgstr "B<-Q>,B< --queue-key >I<msgkey>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-q>,B< --queue-id >I<msgid>"
msgstr "B<-q>,B< --queue-id >I<msgid>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-S>,B< --semaphore-key >I<semkey>"
msgstr "B<-S>,B< --semaphore-key >I<semkey>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-s>,B< --semaphore-id >I<semid>"
msgstr "B<-s>,B< --semaphore-id >I<semid>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#.  .SH AUTHORS
#.  Andre C. Mazzone (linuxdev@karagee.com)
#.  .br
#.  Krishna Balasubramanian (balasub@cis.ohio-state.edu)
#. type: Plain text
#: debian-bullseye
msgid ""
"In its first Linux implementation, B<ipcrm> used the deprecated syntax shown "
"in the second line of the B<SYNOPSIS>.  Functionality present in other *nix "
"implementations of B<ipcrm> has since been added, namely the ability to "
"delete resources by key (not just identifier), and to respect the same "
"command-line syntax.  For backward compatibility the previous syntax is "
"still supported."
msgstr ""
"In der ersten Linux-Implementation von B<ipcrm> wurde die in der zweiten "
"Zeile der B<ÜBERSICHT> angezeigte veraltete Syntax verwendet. Seither wurden "
"die in anderen *nix-Implementationen von B<ipcrm> verfügbaren Funktionen "
"hinzugefügt, insbesondere die Möglichkeit zum Löschen von Ressourcen anhand "
"des Schlüssels (nicht nur anhand des Bezeichners) und die Verwendung der "
"gleichen Befehlszeilensyntax. Aus Gründen der Abwärtskompatibilität wird die "
"alte Syntax weiterhin unterstützt."

#. type: Plain text
#: debian-bullseye
msgid ""
"The ipcrm command is part of the util-linux package and is available from E<."
"UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel Archive E<.UE .>"
msgstr ""
"Der Befehl ipcrm ist Teil des Pakets util-linux, welches aus dem E<.UR "
"https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel-Archiv E<.UE .> heruntergeladen werden kann."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2021-06-02"
msgstr "2. Juni 2021"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
