# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020-2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2022-08-19 19:06+0200\n"
"PO-Revision-Date: 2022-02-07 20:16+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#. type: IX
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "sane-mustek_usb2"
msgstr "sane-mustek_usb2"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "13 Jul 2008"
msgstr "13. Juli 2008"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "sane-mustek_usb2 - SANE backend for SQ113 based USB flatbed scanners"
msgstr ""
"sane-mustek_usb2 - SANE-Backend für SQ113-basierte USB-Flachbettscanner"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide opensuse-tumbleweed
msgid ""
"The B<sane-mustek_usb2> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to USB flatbed scanners based on the Service & "
"Quality SQ113 chipset. At the moment, only the Mustek BearPaw 2448 TA Pro is "
"supported. It's planned to add support for other scanners that are based on "
"the SQ113 and maybe SQ11 chip. For more details, see the B<sane-mustek_usb2> "
"backend homepage: I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>."
msgstr ""
"Die Bibliothek B<sane-mustek_usb2> implementiert ein SANE-(Scanner Access "
"Now Easy) Backend zum Zugriff auf USB-Flachbettscanner, die auf dem Service "
"& Quality SQ113-Chipsatz basieren. Momentan wird nur der Mustek BearPaw 2448 "
"TA Pro unterstützt. Es ist geplant, weitere Scanner zu unterstützen, die auf "
"dem SQ113 und vielleicht dem SQ11 basieren. Weitere Details finden Sie auf "
"der Projektseite des B<mustek_usb2>-Backends: I<http://www.meier-geinitz.de/"
"sane/mustek_usb2-backend/>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is BETA software. Especially if you test new or untested scanners, keep "
"your hand at the scanner's plug and unplug it, if the head bumps at the end "
"of the scan area."
msgstr ""
"Dies ist BETA-Software. Insbesondere dann, wenn Sie neue oder bisher noch "
"nicht getestete Scanner testen, behalten Sie den Netzstecker des Scanners in "
"der Hand und ziehen Sie ihn, falls der Schlitten ans Ende des Scanbereichs "
"stößt."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If you own a scanner other than the ones listed on the mustek_usb2 homepage "
"that works with this backend, please let me know this by sending the "
"scanner's exact model name and the USB vendor and device ids (e.g. from "
"B<sane-find-scanner>(1)  or syslog) to me. Even if the scanner's name is "
"only slightly different from the models already listed as supported, please "
"let me know."
msgstr ""
"Falls Sie einen anderen als die auf der mustek_usb2-Homepage aufgeführten "
"Scanner besitzen, der mit diesem Backend funktioniert, senden Sie mir die "
"genaue Modellbezeichnung des Scanners und die USB-Anbieter- und "
"Gerätekennungen (zum Beispiel mit B<sane-find-scanner>(1) ermittelt oder aus "
"dem Systemprotokoll). Selbst wenn sich der Name des Scanners nur geringfügig "
"von den oben als unterstützt aufgeführten Modellen unterscheidet, lassen Sie "
"es mich bitte wissen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "LIBUSB ISSUES"
msgstr "LIBUSB-PROBLEME"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide opensuse-tumbleweed
msgid ""
"Please use libusb-0.1.8 or later. Without libusb or with older libusb "
"versions all kinds of trouble can be expected. The scanner should be found "
"by B<sane-find-scanner>(1)  without further actions. For setting permissions "
"and general USB information, look at B<sane-usb>(5)."
msgstr ""
"Bitte verwenden Sie libusb-0.1.8 oder neuer. Ohne libusb oder mit älteren "
"libusb-Versionen können Sie mit allem möglichen Ärger rechnen. Der Scanner "
"sollte von B<sane-find-scanner>(1) ohne weiteres Eingreifen gefunden werden. "
"Für das Setzen von Zugriffsrechten und allgemeine USB-Informationen lesen "
"Sie B<sane-usb>(5)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-mustek_usb2.a>"
msgstr "I</usr/lib/sane/libsane-mustek_usb2.a>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Die statische Bibliothek, die dieses Backend implementiert."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-mustek_usb2.so>"
msgstr "I</usr/lib/sane/libsane-mustek_usb2.so>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Die dynamische Bibliothek, die dieses Backend implementiert (auf Systemen "
"verfügbar, die dynamisches Laden unterstützen)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_MUSTEK_USB2>"
msgstr "B<SANE_DEBUG_MUSTEK_USB2>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für dieses Backend. Größere Werte erhöhen "
"die Ausführlichkeit der Ausgabe."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Example: export SANE_DEBUG_MUSTEK_USB2=4"
msgstr "Beispiel: export SANE_DEBUG_MUSTEK_USB2=4"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<sane>(7), B<sane-usb>(5), B<sane-plustek>(5), B<sane-ma1509>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5), B<sane-find-"
"scanner>(1)"
msgstr ""
"B<sane>(7), B<sane-usb>(5), B<sane-plustek>(5), B<sane-ma1509>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5), B<sane-find-"
"scanner>(1)"

#. type: Plain text
#: archlinux
msgid "I</usr/share/doc/sane/mustek_usb2/mustek_usb2.CHANGES>"
msgstr "I</usr/share/doc/sane/mustek_usb2/mustek_usb2.CHANGES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>"
msgstr "I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The driver has been written Roy Zhou, Jack Xu, and Vinci Cen from Mustek."
msgstr ""
"Dieser Treiber wurde von Roy Zhou, Jack Xu und Vinci Cen von Mustek "
"geschrieben."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Adjustments to SANE by Henning Meier-Geinitz."
msgstr "Anpassungen an SANE stammen von Henning Meier-Geinitz."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Please contact me if you find a bug or missing feature: E<lt>I<henning@meier-"
"geinitz.de>E<gt>."
msgstr ""
"Bitte kontaktieren Sie mich, falls Sie einen Fehler entdecken oder wenn Sie "
"eine Funktion vermissen: E<lt>henning@meier-geinitz.deE<gt>."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Please send a debug log if your scanner isn't detected correctly (see "
"B<SANE_DEBUG_MUSTEK_USB2> above)."
msgstr ""
"Senden Sie bitte ein Debug-Protokoll, falls Ihr Scanner nicht richtig "
"erkannt wird (siehe B<SANE_DEBUG_MUSTEK_USB2> oben)."

# FIXME mustek_usb2 → I<mustek_usb2>
#. type: Plain text
#: debian-bullseye mageia-cauldron opensuse-leap-15-4
msgid ""
"The B<sane-mustek_usb2> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to USB flatbed scanners based on the Service & "
"Quality SQ113 chipset. At the moment, only the Mustek BearPaw 2448 TA Pro is "
"supported. It's planned to add support for other scanners that are based on "
"the SQ113 and maybe SQ11 chip. For more details, see the mustek_usb2 backend "
"homepage: I<http://www.meier-geinitz.de/sane/mustek_usb2-backend/>."
msgstr ""
"Die Bibliothek B<sane-mustek_usb2> implementiert ein SANE-(Scanner Access "
"Now Easy) Backend zum Zugriff auf USB-Flachbettscanner, die auf dem Service "
"& Quality SQ113-Chipsatz basieren. Momentan wird nur der Mustek BearPaw 2448 "
"TA Pro unterstützt. Es ist geplant, weitere Scanner zu unterstützen, die auf "
"dem SQ113 und vielleicht dem SQ11 basieren. Weitere Details finden Sie auf "
"der Projektseite des I<mustek_usb2>-Backends: I<http://www.meier-geinitz.de/"
"sane/mustek_usb2-backend/>."

# FIXME I<sane-find-scanner> → B<sane-find-scanner>(1)
#. type: Plain text
#: debian-bullseye
msgid ""
"If you own a scanner other than the ones listed on the mustek_usb2 homepage "
"that works with this backend, please let me know this by sending the "
"scanner's exact model name and the USB vendor and device ids (e.g. from "
"I<sane-find-scanner> or syslog) to me. Even if the scanner's name is only "
"slightly different from the models already listed as supported, please let "
"me know."
msgstr ""
"Falls Sie einen anderen als die auf der mustek_usb2-Homepage aufgeführten "
"Scanner besitzen, der mit diesem Backend funktioniert, senden Sie mir die "
"genaue Modellbezeichnung des Scanners und die USB-Anbieter- und "
"Gerätekennungen (zum Beispiel mit B<sane-find-scanner>(1) ermittelt oder aus "
"dem Systemprotokoll). Selbst wenn sich der Name des Scanners nur geringfügig "
"von den oben als unterstützt aufgeführten Modellen unterscheidet, lassen Sie "
"es mich bitte wissen."

# FIXME sane-find-scanner → B<sane-find-scanner>(1)
# FIXME looks → look
#. type: Plain text
#: debian-bullseye
msgid ""
"Please use libusb-0.1.8 or later. Without libusb or with older libusb "
"versions all kinds of trouble can be expected. The scanner should be found "
"by sane-find-scanner without further actions. For setting permissions and "
"general USB information looks at B<sane-usb>(5)."
msgstr ""
"Bitte verwenden Sie libusb-0.1.8 oder neuer. Ohne libusb oder mit älteren "
"libusb-Versionen können Sie mit allem möglichen Ärger rechnen. Der Scanner "
"sollte von B<sane-find-scanner>(1) ohne weiteres Eingreifen gefunden werden. "
"Für das Setzen von Zugriffsrechten und allgemeine USB-Informationen lesen "
"Sie B<sane-usb>(5)."

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.a>"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-mustek_usb2.so>"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<sane>(7), B<sane-usb>(5), B<sane-plustek>(5), B<sane-ma1509>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5)"
msgstr ""
"B<sane>(7), B<sane-usb>(5), B<sane-plustek>(5), B<sane-ma1509>(5), B<sane-"
"mustek_usb>(5), B<sane-mustek>(5), B<sane-mustek_pp>(5)"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "I</usr/share/doc/libsane/mustek_usb2/mustek_usb2.CHANGES>"
msgstr "I</usr/share/doc/libsane/mustek_usb2/mustek_usb2.CHANGES>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The driver has been written Roy Zhou, Jack Xu, and Vinci Cen from Mustek. "
"Adjustments to SANE by Henning Meier-Geinitz."
msgstr ""
"Dieser Treiber wurde von Roy Zhou, Jack Xu und Vinci Cen von Mustek "
"geschrieben. Anpassungen an SANE stammen von Henning Meier-Geinitz."

# FIXME SANE_DEBUG_MUSTEK_USB2 → B<SANE_DEBUG_MUSTEK_USB2>
#. type: Plain text
#: debian-bullseye
msgid ""
"Please contact me if you find a bug or missing feature: E<lt>henning@meier-"
"geinitz.deE<gt>. Please send a debug log if your scanner isn't detected "
"correctly (see SANE_DEBUG_MUSTEK_USB2 above)."
msgstr ""
"Bitte kontaktieren Sie mich, falls Sie einen Fehler entdecken oder wenn Sie "
"eine Funktion vermissen: E<lt>henning@meier-geinitz.deE<gt>. Senden Sie "
"bitte ein Debug-Protokoll, falls Ihr Scanner nicht richtig erkannt wird "
"(siehe B<SANE_DEBUG_MUSTEK_USB2> oben)."

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-mustek_usb2.a>"
msgstr "I</usr/lib64/sane/libsane-mustek_usb2.a>"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-mustek_usb2.so>"
msgstr "I</usr/lib64/sane/libsane-mustek_usb2.so>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "I</usr/share/doc/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"
msgstr "I</usr/share/doc/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"

# FIXME looks → look
#. type: Plain text
#: mageia-cauldron opensuse-leap-15-4
msgid ""
"Please use libusb-0.1.8 or later. Without libusb or with older libusb "
"versions all kinds of trouble can be expected. The scanner should be found "
"by B<sane-find-scanner>(1)  without further actions. For setting permissions "
"and general USB information looks at B<sane-usb>(5)."
msgstr ""
"Bitte verwenden Sie libusb-0.1.8 oder neuer. Ohne libusb oder mit älteren "
"libusb-Versionen können Sie mit allem möglichen Ärger rechnen. Der Scanner "
"sollte von B<sane-find-scanner>(1) ohne weiteres Eingreifen gefunden werden. "
"Für das Setzen von Zugriffsrechten und allgemeine USB-Informationen lesen "
"Sie B<sane-usb>(5)."

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"I</usr/share/doc/packages/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"
msgstr ""
"I</usr/share/doc/packages/sane-backends/mustek_usb2/mustek_usb2.CHANGES>"
