# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2022-08-19 19:06+0200\n"
"PO-Revision-Date: 2022-02-06 08:08+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#. type: IX
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "sane-kodakaio"
msgstr "sane-kodakaio"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "17 Jun 2012"
msgstr "17. Juni 2012"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "sane-kodakaio - SANE backend for Kodak aio printer / scanners"
msgstr "sane-kodakaio - SANE-Backend für AiO-Drucker/-Scanner von Kodak"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<sane-kodakaio> library implements a SANE (Scanner Access Now Easy) "
"backend which provides access to Kodak aio printer/scanners, like the ESP "
"and Hero series."
msgstr ""
"Die Bibliothek B<sane-kodakaio> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf die AiO-Drucker/-Scanner von Kodak, wie die "
"ESP- und Hero-Baureihen."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This document describes backend version 2.4, which is the first candidate "
"for incorporation in sane-backends."
msgstr ""
"Dieses Dokument beschreibt das Backend in der Version 2.4, welches der erste "
"Kandidat für die Einbeziehung in sane-backends ist."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SUPPORTED HARDWARE"
msgstr "UNTERSTÜTZTE HARDWARE"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide opensuse-tumbleweed
msgid ""
"This version should support models of the Kodak ESP and Hero series, and "
"possibly some Advent AiO scanners. The ESP 5250 and Hero 9.1 were used to "
"develop the backend, but other models may work. Please see the supported "
"devices list at I<http://www.sane-project.org/sane-backends.html#S-KODAKAIO>."
msgstr ""
"Diese Version sollte Modelle der ESP- und Hero-Baureihen von Kodak sowie "
"möglicherweise einige Advent-AiO-Scanner. Zum Entwickeln des Backends wurden "
"der ESP 5250 und der Hero 9.1 verwendet, aber andere Modelle könnten auch "
"funktionieren. Eine Liste der unterstützten Geräte finden Sie auf I<http://"
"www.sane-project.org/sane-backends.html#S-KODAKAIO>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If you have a model not on that list, or reported as 'untested': the best "
"way to determine level of support is to test the scanner directly."
msgstr ""
"Falls Sie ein Modell haben, das nicht in der Liste aufgeführt oder als "
"»untested« bezeichnet ist, dann ist der beste Weg zur Ermittlung des "
"Unterstützungsumfangs, den Scanner direkt zu testen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "KONFIGURATIONSDATEI"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The configuration file I<kodakaio.conf> is used to tell the backend how to "
"look for scanners, and provide options controlling the operation of the "
"backend.  This file is read each time the frontend asks the backend for a "
"list of scanners, generally only when the frontend starts."
msgstr ""
"Aus der Konfigurationsdatei I<kodakaio.conf> ermittelt das Backend, wie nach "
"Scannern gesucht werden soll. Außerdem stellt sie Optionen zur Steuerung der "
"Aktionen des Backends bereit. Diese Datei wird immer dann gelesen, wenn das "
"Frontend vom Backend eine Liste der Scanner anfordert, im Allgemeinen nur, "
"wenn das Frontend gestartet wird."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_KODAKAIO>"
msgstr "B<SANE_DEBUG_KODAKAIO>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für dieses Backend. Größere Werte erhöhen "
"die Ausführlichkeit der Ausgabe."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "KNOWN ISSUES"
msgstr "BEKANNTE PROBLEME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Most hardware options are either not supported or not exposed for control by "
"the user, including: multifeed detection, image compression etc."
msgstr ""
"Die meisten Hardwareoptionen werden entweder nicht unterstützt oder können "
"vom Benutzer nicht direkt gesteuert werden. Dies betrifft die Erkennung von "
"Mehrfacheinzügen, die Bildkompression usw."

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<sane>(7)"
msgstr "B<sane>(7)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "P. Newall"
msgstr "P. Newall"

#. type: Plain text
#: debian-bullseye
msgid ""
"The B<sane-kodakaio> library implements a SANE (Scanner Access Now Easy) "
"backend which provides access to Kodak aio printer / scanners, like the ESP "
"and Hero series."
msgstr ""
"Die Bibliothek B<sane-kodakaio> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf die AiO-Drucker/-Scanner von Kodak, wie die "
"ESP- und Hero-Baureihen."

# FIXME where to find the desc file?
#. type: Plain text
#: debian-bullseye mageia-cauldron opensuse-leap-15-4
msgid ""
"This version should support models of the Kodak ESP and Hero series, and "
"possibly some Advent AiO scanners. The ESP 5250 and Hero 9.1 were used to "
"develop the backend, but other models may work. Please see the desc file."
msgstr ""
"Diese Version sollte Modelle der ESP- und Hero-Baureihen von Kodak sowie "
"möglicherweise einige Advent-AiO-Scanner. Zum Entwickeln des Backends wurden "
"der ESP 5250 und der Hero 9.1 verwendet, aber andere Modelle könnten auch "
"funktionieren. Bitte schauen Sie in die Beschreibungsdatei."

# FIXME "kodakaio.conf" → I<kodakaio.conf>
#. type: Plain text
#: debian-bullseye
msgid ""
"The configuration file \"kodakaio.conf\" is used to tell the backend how to "
"look for scanners, and provide options controlling the operation of the "
"backend.  This file is read each time the frontend asks the backend for a "
"list of scanners, generally only when the frontend starts."
msgstr ""
"Aus der Konfigurationsdatei I<kodakaio.conf> ermittelt das Backend, wie nach "
"Scannern gesucht werden soll. Außerdem stellt sie Optionen zur Steuerung der "
"Aktionen des Backends bereit. Diese Datei wird immer dann gelesen, wenn das "
"Frontend vom Backend eine Liste der Scanner anfordert, im Allgemeinen nur, "
"wenn das Frontend gestartet wird."

# FIXME SANE_DEBUG_KODAKAIO → B<SANE_DEBUG_KODAKAIO>
#. type: Plain text
#: debian-bullseye
msgid ""
"The backend uses a single environment variable, SANE_DEBUG_KODAKAIO, which "
"enables debugging output to stderr."
msgstr ""
"Das Backend verwendet B<SANE_DEBUG_KODAKAIO> als einzige Umgebungsvariable, "
"welche bewirkt, dass Debugging-Ausgaben in die Standardfehlerausgabe "
"geschrieben werden."
