# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2022-08-19 19:19+0200\n"
"PO-Revision-Date: 2022-02-11 11:46+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "WDCTL"
msgstr "WDCTL"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "wdctl - show hardware watchdog status"
msgstr "wdctl - den Watchdog-Status der Hardware anzeigen"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<wdctl> [options] [I<device>...]"
msgstr "B<wdctl> [Optionen] [I<Gerät> …]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Show hardware watchdog status. The default device is I</dev/watchdog>. If "
"more than one device is specified then the output is separated by one blank "
"line."
msgstr ""
"Das Programm zeigt den Watchdog-Status der Hardware an. Das voreingestellte "
"Gerät ist I</dev/watchdog>. Falls mehr als ein Gerät angegeben wird, dann "
"wird die Ausgabe durch Leerzeilen getrennt."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If the device is already used or user has no permissions to read from the "
"device, then B<wdctl> reads data from sysfs. In this case information about "
"supported features (flags) might be missing."
msgstr ""
"Falls das Gerät bereits in Benutzung ist oder der Benutzer nicht über die "
"notwendigen Lesezugriffsrechte für das Gerät verfügt, dann liest B<wdctl> "
"Daten aus Sysfs. In diesem Fall könnten die Informationen zu unterstützten "
"Funktionsmerkmalen (Flags) fehlen."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Note that the number of supported watchdog features is hardware specific."
msgstr ""
"Beachten Sie, dass die Zahl der verfügbaren Watchdog-Funktionsmerkmale von "
"der Hardware abhängt."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-f>, B<--flags> I<list>"
msgstr "B<-f>, B<--flags> I<Liste>"

# Flags sind hier keine Schalter im Sinne von Befehlsparametern
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Print only the specified flags."
msgstr "gibt nur die angegebenen Flags aus."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-F>, B<--noflags>"
msgstr "B<-F>, B<--noflags>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Do not print information about flags."
msgstr "gibt keine Informationen zu Flags aus."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-I>, B<--noident>"
msgstr "B<-I>, B<--noident>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Do not print watchdog identity information."
msgstr "gibt keine Watchdog-Identitätsinformationen aus."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Do not print a header line for flags table."
msgstr "gibt keine Kopfzeile für die Flags-Tabelle aus."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<Liste>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Define the output columns to use in table of watchdog flags. If no output "
"arrangement is specified, then a default set is used. Use B<--help> to get "
"list of all supported columns."
msgstr ""
"definiert die Ausgabespalten, die in der Tabelle der Watchdog-Flags "
"angezeigt werden sollen. Falls kein Argument für die Ausgabe angegeben wird, "
"dann wird ein vorgegebener Spaltensatz verwendet. Mit B<--help> erhalten Sie "
"eine Liste der unterstützten Spalten."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-O>, B<--oneline>"
msgstr "B<-O>, B<--oneline>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Print all wanted information on one line in key=\"value\" output format."
msgstr ""
"gibt alle gewünschten Informationen in einer Zeile im Format "
"Schlüssel=\"Wert\" aus."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "B<-p>, B<--setpretimeout> I<seconds>"
msgstr "B<-p>, B<--setpretimeout> I<Sekunden>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"Set the watchdog pre-timeout in seconds. A watchdog pre-timeout is a "
"notification generated by the watchdog before the watchdog reset might occur "
"in the event the watchdog has not been serviced. This notification is "
"handled by the kernel and can be configured to take an action using sysfs or "
"by B<--setpregovernor>."
msgstr ""
"setzt die Vor-Zeitüberschreitung des Watchdogs in Sekunden Eine solche Vor-"
"Zeitüberschreitung ist eine vom Watchdog erzeugte Benachrichtigung, bevor "
"der Watchdog zurückgesetzt werden würde, falls dieser gar nicht "
"bereitgestellt worden wäre. Diese Benachrichtigung wird vom Kernel "
"gehandhabt. Mittels Sysfs oder mit der Option B<--setpregovernor> kann eine "
"daraufhin auszuführende Aktion eingerichtet werden."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "B<-g>, B<--setpregovernor> I<governor>"
msgstr "B<-g>, B<--setpregovernor> I<Governor>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"Set pre-timeout governor name. For available governors see default B<wdctl> "
"output."
msgstr ""
"setzt den Namen des Governors für Vor-Zeitüberschreitung. Verfügbare "
"Governors finden Sie in der standardmäßigen Ausgabe von B<wdctl>."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Use the raw output format."
msgstr "verwendet das Rohformat für die Ausgabe."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "B<-s>, B<--settimeout> I<seconds>"
msgstr "B<-s>, B<--settimeout> I<Sekunden>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Set the watchdog timeout in seconds."
msgstr "legt die Watchdog-Zeitüberschreitung in Sekunden fest."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-T>, B<--notimeouts>"
msgstr "B<-T>, B<--notimeouts>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Do not print watchdog timeouts."
msgstr "gibt keine Watchdog-Zeitüberschreitungen aus."

#. #-#-#-#-#  archlinux: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: IP
#. #-#-#-#-#  debian-unstable: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-37: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: wdctl.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--flags-only>"
msgstr "B<-x>, B<--flags-only>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Same as B<-I -T>."
msgstr "ist gleichbedeutend mit B<-I -T>."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<wdctl> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<wdctl> ist Teil des Pakets util-linux, welches heruntergeladen "
"werden kann von:"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "July 2014"
msgstr "Juli 2014"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid ""
"Show hardware watchdog status.  The default device is I</dev/watchdog>.  If "
"more than one device is specified then the output is separated by one blank "
"line."
msgstr ""
"Das Programm zeigt den Watchdog-Status der Hardware an. Das voreingestellte "
"Gerät ist I</dev/watchdog>. Falls mehr als ein Gerät angegeben wird, dann "
"wird die Ausgabe durch Leerzeilen getrennt."

# FIXME than → then
#. type: Plain text
#: debian-bullseye
msgid ""
"If the device is already used or user has no permissions to read from the "
"device than B<wdctl> reads data from sysfs.  In this case information about "
"supported features (flags) might be missing."
msgstr ""
"Falls das Gerät bereits in Benutzung ist oder der Benutzer nicht über die "
"notwendigen Lesezugriffsrechte für das Gerät verfügt, dann liest B<wdctl> "
"Daten aus Sysfs. In diesem Fall könnten die Informationen zu unterstützten "
"Funktionsmerkmalen (Flags) fehlen."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-f>,B< --flags >I<list>"
msgstr "B<-f>,B< --flags >I<Liste>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-F>,B< --noflags>"
msgstr "B<-F>,B< --noflags>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-I>,B< --noident>"
msgstr "B<-I>,B< --noident>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-n>,B< --noheadings>"
msgstr "B<-n>,B< --noheadings>"

#. type: IP
#: debian-bullseye
#, no-wrap
msgid "B<-o>, B<--output >I<list>"
msgstr "B<-o>, B<--output >I<Liste>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Define the output columns to use in table of watchdog flags.  If no output "
"arrangement is specified, then a default set is used.  Use B<--help> to get "
"list of all supported columns."
msgstr ""
"definiert die Ausgabespalten, die in der Tabelle der Watchdog-Flags "
"angezeigt werden sollen. Falls kein Argument für die Ausgabe angegeben wird, "
"dann wird ein vorgegebener Spaltensatz verwendet. Mit B<--help> erhalten Sie "
"eine Liste der unterstützten Spalten."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-O>,B< --oneline>"
msgstr "B<-O>,B< --oneline>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-r>,B< --raw>"
msgstr "B<-r>,B< --raw>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-s>,B< -settimeout >I<seconds>"
msgstr "B<-s>,B< -settimeout >I<Sekunden>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-T>,B< --notimeouts>"
msgstr "B<-T>,B< --notimeouts>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>,B< --version>"
msgstr "B<-V>,B< --version>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: Plain text
#: debian-bullseye
msgid "E<.MT kzak@\\:redhat\\:.com> Karel Zak E<.ME>"
msgstr "E<.MT kzak@\\:redhat\\:.com> Karel Zak E<.ME>"

#. type: Plain text
#: debian-bullseye
msgid "E<.MT lennart@\\:poettering\\:.net> Lennart Poettering E<.ME>"
msgstr "E<.MT lennart@\\:poettering\\:.net> Lennart Poettering E<.ME>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The B<wdctl> command is part of the util-linux package and is available from "
"E<.UR https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> "
"Linux Kernel Archive E<.UE .>"
msgstr ""
"Der Befehl B<wdctl> ist Teil des Pakets util-linux, welches aus dem E<.UR "
"https://\\:www.kernel.org\\:/pub\\:/linux\\:/utils\\:/util-linux/> Linux "
"Kernel-Archiv E<.UE .> heruntergeladen werden kann."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2021-06-02"
msgstr "2. Juni 2021"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-s>, B<-settimeout> I<seconds>"
msgstr "B<-s>, B<-settimeout> I<Sekunden>"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
