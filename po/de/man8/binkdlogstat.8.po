# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010-2011.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2021-08-27 16:59+0200\n"
"PO-Revision-Date: 2021-01-23 21:31+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "BINKDLOGSTAT"
msgstr "BINKDLOGSTAT"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "July 2005"
msgstr "Juli 2005"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

# FIXME: statistics
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Binkdlogstat - binkd log analyser and statistic generator"
msgstr "Binkdlogstat - analysiert das Binkd-Protokoll und erstellt Statistiken"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSYS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<binkdlogstat E<lt> >E<lt>I<binkd-log-file>E<gt>"
msgstr "B<binkdlogstat E<lt> >E<lt>I<binkd-log-file>E<gt>"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<binkdlogstat> is a program which generates reports of the activity of the "
"binkd daemon. It analyzes binkd log files and prints to stdout a formatted "
"report with the following information for each node:"
msgstr ""
"B<Binkdlogstat> erzeugt Berichte über die Arbeit des B<binkd>(8)-Daemons . "
"Das Programm analysiert B<binkd>(8)-Protokolldateien und gibt einen "
"formatierten Bericht auf der Standardausgabe aus. Für jeden Knoten sind die "
"folgenden Informationen enthalten:"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- number of successful incoming sessions"
msgstr "- Anzahl der erfolgreichen eingehenden Sitzungen"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- number of failed incoming sessions"
msgstr "- Anzahl der fehlgeschlagenen eingehenden Sitzungen"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- number of successful outgoing sessions"
msgstr "- Anzahl der erfolgreichen ausgehenden Sitzungen"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- number of failed outgoing sessions"
msgstr "- Anzahl der fehlgeschlagenen ausgehenden Sitzungen"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- outgoing traffic in bytes"
msgstr "- ausgehender Verkehr in Byte"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- incoming traffic in bytes"
msgstr "- eingehender Verkehr in Byte"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- number of sent files"
msgstr "- Anzahl gesendeter Dateien"

# Hier wird von Sitzungen geredet - aber sind es nicht im Prinzip
# Verbindungen?
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- number of received files"
msgstr "- Anzahl empfangener Dateien"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "- average CPS with node"
msgstr "- durchschnittliche CPS pro Knoten"

# FIXME: Bug Information
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"It also provides total information for these categories for the log file."
msgstr ""
"Außerdem stellt es auch Gesamtzahlen für diese Kategorien für die "
"Protokolldatei bereit."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<binkdlogstat> uses stdin as a source and stdout as a result file, so it "
"can be used as a filter."
msgstr ""
"B<Binkdlogstat> verwendet die Standardeingabe als Quelle und die "
"Standardausgabe als Ergebnisdatei. Daher kann es als Filter eingesetzt "
"werden."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "binkd(8)"
msgstr "B<binkd>(8)"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Binkdlogstat was written by Pavel Gulchouck E<lt>gul@lucky.netE<gt> 2:463/68"
msgstr ""
"B<Binkdlogstat> wurde von Pavel Gulchouck E<lt>gul@lucky.netE<gt> 2:463/68 "
"geschrieben."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This manual page was written by Kaliuta Yauheni E<lt>y.kaliuta@gmail.comE<gt>"
msgstr ""
"Diese Handbuchseite erstellte Kaliuta Yauheni E<lt>y.kaliuta@gmail.comE<gt>."

# Wo gibt es offizielle Übersetzungen?
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This is free documentation; you can redistribute it and/or modify it under "
"the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version."
msgstr ""
"Dieses Handbuch ist Freie Dokumentation. Sie können es unter den Bedingungen "
"der GNU General Public License in der von der Free Software Foundation "
"veröffentlichten Fassung weiterverteilen. Dabei haben Sie die Wahl zwischen "
"der Version 2 der Lizenz oder irgendeiner späteren Version."
