# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Horváth András <horvatha@rs1.szif.hu>, 2001.
# Tímár András <timar_a@freemail.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 19:18+0200\n"
"PO-Revision-Date: 2021-05-18 20:32+0200\n"
"Last-Translator: Tímár András <timar_a@freemail.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "WC"
msgstr "WC"

#. type: TH
#: archlinux
#, no-wrap
msgid "April 2022"
msgstr "2022 április"

#. type: TH
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Felhasználói parancsok"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "wc - print the number of bytes, words, and lines in files"
msgid "wc - print newline, word, and byte counts for each file"
msgstr "wc - fájlokban található bájtok, szavak és sorok számát írja ki"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<wc> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<wc> [I<\\,KAPCSOLÓ\\/>]... [I<\\,FÁJL\\/>]..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<wc> [I<\\,OPTION\\/>]... I<\\,--files0-from=F\\/>"
msgstr "B<wc> [I<\\,KAPCSOLÓ\\/>]... [I<\\,--files0-from=F\\/>]..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Print newline, word, and byte counts for each FILE, and a total line if "
#| "more than one FILE is specified.  A word is a non-zero-length sequence of "
#| "characters delimited by white space."
msgid ""
"Print newline, word, and byte counts for each FILE, and a total line if more "
"than one FILE is specified.  A word is a non-zero-length sequence of "
"printable characters delimited by white space."
msgstr ""
"Új sor, szó- és bájtszámokat ír ki minden egyes FÁJLHOZ, és a sorok számának "
"összesítését, ha több FÁJL van megadva. A szó egy nem nulla hosszú "
"karaktersorozat, amelyet üres hely karakterek határolnak."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Ha a FÁJL nincs megadva, vagy -, akkor a szabványos bemenetet olvassa."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The options below may be used to select which counts are printed, always in "
"the following order: newline, word, character, byte, maximum line length."
msgstr ""
"Az alábbi kapcsolókkal kiválaszthatók a kinyomtatandó számok, mindig a "
"következő sorrendben: újsor, szó, karakter, bájt, maximális sorhossz."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--bytes>"
msgstr "B<-c>, B<--bytes>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "print the byte counts"
msgstr "a bájtok számának kiírása"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--chars>"
msgstr "B<-m>, B<--chars>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "print the character counts"
msgstr "a karakterek számának kiírása"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--lines>"
msgstr "B<-l>, B<--lines>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "print the newline counts"
msgstr "az új sorok számának kiírása"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--files0-from>=I<\\,F\\/>"
msgstr "B<--files0-from>=I<\\,F\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"read input from the files specified by NUL-terminated names in file F; If F "
"is - then read names from standard input"
msgstr ""
"bemenet olvasása az F fájlban található, NULL végű nevek által megadott "
"fájlokból. Ha az F a -, akkor a szabványos bemenetről olvassa a neveket"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>, B<--max-line-length>"
msgstr "B<-L>, B<--max-line-length>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "print the maximum display width"
msgstr "a maximális megjelenítési szélesség kiírása"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--words>"
msgstr "B<-w>, B<--words>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "print the word counts"
msgstr "szószámok kiírása"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "ezen súgó megjelenítése és kilépés"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "verzióinformációk megjelenítése és kilépés"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "SZERZŐ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by Paul Rubin and David MacKenzie."
msgstr "Írta Paul Rubin és David MacKenzie."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HIBÁK JELENTÉSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"A(z) GNU coreutils online súgója: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "SZERZŐI JOG"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  A licenc GPLv3+: a GNU "
"GPL 3. vagy újabb változata: E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ez egy szabad szoftver, terjesztheti és/vagy módosíthatja. NINCS GARANCIA, a "
"törvény által engedélyezett mértékig."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/wcE<gt>"
msgstr ""
"Teljes dokumentáció E<lt>https://www.gnu.org/software/coreutils/wcE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) wc invocation\\(aq"
msgstr "vagy helyileg elérhető: info \\(aq(coreutils) wc invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "2020 szeptember"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Print newline, word, and byte counts for each FILE, and a total line if more "
"than one FILE is specified.  A word is a non-zero-length sequence of "
"characters delimited by white space."
msgstr ""
"Új sor, szó- és bájtszámokat ír ki minden egyes FÁJLHOZ, és a sorok számának "
"összesítését, ha több FÁJL van megadva. A szó egy nem nulla hosszú "
"karaktersorozat, amelyet üres hely karakterek határolnak."

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  A licenc GPLv3+: a GNU "
"GPL 3. vagy újabb változata: E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "2021 október"

#. type: TH
#: fedora-37 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "August 2021"
msgid "August 2022"
msgstr "2021. augusztus"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "2021 szeptember"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "GNU coreutils 9.0"

#. type: Plain text
#: mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  A licenc GPLv3+: a GNU "
"GPL 3. vagy újabb változata: E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
