# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Horváth András <horvatha@rs1.szif.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 18:54+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Horváth András <horvatha@rs1.szif.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "KILL"
msgstr "KILL"

#. type: TH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-05-11"
msgstr "2022. május 11"

#. type: TH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "util-linux 2.38"
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38"

#. type: TH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Felhasználói parancsok"

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "kill - terminate a process"
msgstr "kill - megállít egy processzt"

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"B<kill> [B<->I<signal>|B<-s> I<signal>|B<-p>] [B<-q> I<value>] [B<-a>] [B<--"
"timeout> I<milliseconds> I<signal>] [B<-->] I<pid>|I<name>..."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<kill> B<-l> [I<number>] | B<-L>"
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The command B<kill> sends the specified I<signal> to the specified processes "
"or process groups."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"If no signal is specified, the B<TERM> signal is sent. The default action "
"for this signal is to terminate the process. This signal should be used in "
"preference to the B<KILL> signal (number 9), since a process may install a "
"handler for the TERM signal in order to perform clean-up steps before "
"terminating in an orderly fashion. If a process does not terminate after a "
"B<TERM> signal has been sent, then the B<KILL> signal may be used; be aware "
"that the latter signal cannot be caught, and so does not give the target "
"process the opportunity to perform any clean-up before terminating."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Most modern shells have a builtin B<kill> command, with a usage rather "
"similar to that of the command described here. The B<--all>, B<--pid>, and "
"B<--queue> options, and the possibility to specify processes by command "
"name, are local extensions."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"If I<signal> is 0, then no actual signal is sent, but error checking is "
"still performed."
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "ARGUMENTS"
msgstr "ÉRTÉKEK"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The list of processes to be signaled can be a mixture of names and PIDs."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "I<pid>"
msgstr "I<pid>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Each I<pid> can be expressed in one of the following ways:"
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "I<n>"
msgstr "I<n>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "where I<n> is larger than 0. The process with PID I<n> is signaled."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "All processes in the current process group are signaled."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "All processes with a PID larger than 1 are signaled."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<->I<n>"
msgstr "B<->I<n>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"where I<n> is larger than 1. All processes in process group I<n> are "
"signaled. When an argument of the form \\(aq-n\\(aq is given, and it is "
"meant to denote a process group, either a signal must be specified first, or "
"the argument must be preceded by a \\(aq--\\(aq option, otherwise it will be "
"taken as the signal to send."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "I<name>"
msgstr "I<név>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "All processes invoked using this I<name> will be signaled."
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "KAPCSOLÓK"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<-d>, B<--minimal>"
msgid "B<-s>, B<--signal> I<signal>"
msgstr "B<-d>, B<--minimal>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Specify the signal to send.  The signal may be given as a signal name or "
#| "number."
msgid "The signal to send. It may be given as a name or a number."
msgstr "Megadja a küldendő jelzést. A jelzés névvel vagy számmal is megadható."

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<-l>, B<--link>"
msgid "B<-l>, B<--list> [I<number>]"
msgstr "B<-l>, B<--link>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Print a list of signal names.  These are found in I</usr/include/linux/"
#| "signal.h>"
msgid ""
"Print a list of signal names, or convert the given signal number to a name. "
"The signals can be found in I</usr/include/linux/signal.h>."
msgstr ""
"Listát készít a lehetséges jelzésnevekről. Ezeket a I</usr/include/linux/"
"signal.h> -ban találhatjuk meg egyébként."

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<-L>, B<--table>"
msgstr "B<-L>, B<--table>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Similar to B<-l>, but it will print signal names and their corresponding "
"numbers."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Do not restrict the command-name-to-PID conversion to processes with the "
"same UID as the present process."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<-p>, B<--pid>"
msgstr "B<-p>, B<--pid>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Specify that B<kill> should only print the process id I<(pid)> of the "
#| "named process, and should not send it a signal."
msgid ""
"Only print the process ID (PID) of the named processes, do not send any "
"signals."
msgstr ""
"Azt eredményezi, hogy a B<kill> csak kiírja az érintett PID értékeket, de "
"nem küldi el a jelzést."

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<--verbose>"
msgstr "B<--verbose>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Print PID(s) that will be signaled with B<kill> along with the signal."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<-q>, B<--quiet>"
msgid "B<-q>, B<--queue> I<value>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Send the signal using B<sigqueue>(3) rather than B<kill>(2). The I<value> "
"argument is an integer that is sent along with the signal. If the receiving "
"process has installed a handler for this signal using the B<SA_SIGINFO> flag "
"to B<sigaction>(2), then it can obtain this data via the I<si_sigval> field "
"of the I<siginfo_t> structure."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<--timeout> I<milliseconds signal>"
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Send a signal defined in the usual way to a process, followed by an "
"additional signal after a specified delay. The B<--timeout> option causes "
"B<kill> to wait for a period defined in I<milliseconds> before sending a "
"follow-up I<signal> to the process. This feature is implemented using the "
"Linux kernel PID file descriptor feature in order to guarantee that the "
"follow-up signal is sent to the same process or not sent if the process no "
"longer exists."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Note that the operating system may re-use PIDs and implementing an "
"equivalent feature in a shell using B<kill> and B<sleep> would be subject to "
"races whereby the follow-up signal might be sent to a different process that "
"used a recycled PID."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The B<--timeout> option can be specified multiple times: the signals are "
"sent sequentially with the specified timeouts. The B<--timeout> option can "
"be combined with the B<--queue> option."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"As an example, the following command sends the signals B<QUIT>, B<TERM> and "
"B<KILL> in sequence and waits for 1000 milliseconds between sending the "
"signals:"
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"kill --verbose --timeout 1000 TERM --timeout 1000 KILL \\(rs\n"
"        --signal QUIT 12345\n"
msgstr ""
"kill --verbose --timeout 1000 TERM --timeout 1000 KILL \\(rs\n"
"        --signal QUIT 12345\n"

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "KILÉPÉSI ÁLLAPOT"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<kill> has the following exit status values:"
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "success"
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "failure"
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<64>"
msgstr "B<64>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "partial success (when more than one process specified)"
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "MEGJEGYZÉS"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Although it is possible to specify the TID (thread ID, see B<gettid>(2)) of "
"one of the threads in a multithreaded process as the argument of B<kill>, "
"the signal is nevertheless directed to the process (i.e., the entire thread "
"group). In other words, it is not possible to send a signal to an explicitly "
"selected thread in a multithreaded process. The signal will be delivered to "
"an arbitrarily selected thread in the target process that is not blocking "
"the signal. For more details, see B<signal>(7) and the description of "
"B<CLONE_THREAD> in B<clone>(2)."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Various shells provide a builtin B<kill> command that is preferred in "
"relation to the B<kill>(1) executable described by this manual. The easiest "
"way to ensure one is executing the command described in this page is to use "
"the full path when calling the command, for example: B</bin/kill --version>"
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "SZERZŐI"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "The original version was taken from BSD 4.4."
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"B<bash>(1), B<tcsh>(1), B<sigaction>(2), B<kill>(2), B<sigqueue>(3), "
"B<signal>(7)"
msgstr ""
"B<bash>(1), B<tcsh>(1), B<sigaction>(2), B<kill>(2), B<sigqueue>(3), "
"B<signal>(7)"

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HIBÁK JELENTÉSE"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "ELÉRHETŐSÉG"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The B<kill> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2021-06-02"
msgstr "2021. június 2"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<kill> [-signal|B<-s> I<signal>|B<-p>] [B<-q> I<value>] [B<-a>] [B<--"
"timeout> I<milliseconds> I<signal>] [B<-->] I<pid>|I<name>..."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If no signal is specified, the TERM signal is sent. The default action for "
"this signal is to terminate the process. This signal should be used in "
"preference to the KILL signal (number 9), since a process may install a "
"handler for the TERM signal in order to perform clean-up steps before "
"terminating in an orderly fashion. If a process does not terminate after a "
"TERM signal has been sent, then the KILL signal may be used; be aware that "
"the latter signal cannot be caught, and so does not give the target process "
"the opportunity to perform any clean-up before terminating."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"As an example, the following command sends the signals QUIT, TERM and KILL "
"in sequence and waits for 1000 milliseconds between sending the signals:"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-02-14"
msgstr "2022. február 14"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
