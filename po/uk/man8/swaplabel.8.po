# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 19:11+0200\n"
"PO-Revision-Date: 2022-07-13 20:02+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SWAPLABEL"
msgstr "SWAPLABEL"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "swaplabel - print or change the label or UUID of a swap area"
msgstr "swaplabel — показ або зміна мітки або UUID області резервної пам'яті."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<swaplabel> [B<-L> I<label>] [B<-U> I<UUID>] I<device>"
msgstr "B<swaplabel> [B<-L> I<мітка>] [B<-U> I<UUID>] I<пристрій>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<swaplabel> will display or change the label or UUID of a swap partition "
"located on I<device> (or regular file)."
msgstr ""
"B<swaplabel> покаже або надасть змогу змінити мітку або UUID розділу "
"резервної пам'яті на диску, який розташовано на пристрої I<пристрій> (або у "
"звичайному файлі)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If the optional arguments B<-L> and B<-U> are not given, B<swaplabel> will "
"simply display the current swap-area label and UUID of I<device>."
msgstr ""
"Якщо не вказано необов'язкових параметрів B<-L> і B<-U>, B<swaplabel> просто "
"виведе поточні значення мітки і UUID розділу резервної пам'яті на диску на "
"пристрої I<пристрій>."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If an optional argument is present, then B<swaplabel> will change the "
"appropriate value on I<device>. These values can also be set during swap "
"creation using B<mkswap>(8). The B<swaplabel> utility allows changing the "
"label or UUID on an actively used swap device."
msgstr ""
"Якщо вказано необов'язковий параметр, B<swaplabel> змінить відповідне "
"значення на пристрої I<пристрій>. Ці значення також може бути встановлено "
"під час створення резервної пам'яті на диску за допомогою програми "
"B<mkswap>(8). Допоміжна програма B<swaplabel> надає змогу змінювати мітку "
"або UUID на активному використаному пристрої резервної пам'яті."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-L>, B<--label> I<label>"
msgstr "B<-L>, B<--label> I<мітка>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Specify a new I<label> for the device. Swap partition labels can be at most "
"16 characters long. If I<label> is longer than 16 characters, B<swaplabel> "
"will truncate it and print a warning message."
msgstr ""
"Вказати нову I<мітку> пристрою. Мітки розділів резервної пам'яті на диску не "
"повинна перевищувати у довжину 16 символів. Якщо довжина I<мітки> "
"перевищуватиме 16 символів, B<swaplabel> обріже її і виведе попередження."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-U>, B<--uuid> I<UUID>"
msgstr "B<-U>, B<--uuid> I<UUID>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Specify a new I<UUID> for the device. The I<UUID> must be in the standard "
"8-4-4-4-12 character format, such as is output by B<uuidgen>(1)."
msgstr ""
"Вказати новий I<UUID> для пристрою. I<UUID> має бути вказано у стандартному "
"форматі — 8-4-4-4-12 символів, такому самому, у якому виводить дані "
"B<uuidgen>(1)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "СЕРЕДОВИЩЕ"

#. #-#-#-#-#  archlinux: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bullseye: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: IP
#. #-#-#-#-#  debian-unstable: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-37: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: swaplabel.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "LIBBLKID_DEBUG=all"
msgstr "LIBBLKID_DEBUG=all"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "enables libblkid debug output."
msgstr "вмикає показ діагностичних повідомлень libblkid."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<swaplabel> was written by"
msgstr "Авторами B<swaplabel> є"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "and"
msgstr "і"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<uuidgen>(1), B<mkswap>(8), B<swapon>(8)"
msgstr "B<uuidgen>(1), B<mkswap>(8), B<swapon>(8)"

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<swaplabel> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<swaplabel> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "April 2010"
msgstr "Квітень 2010 року"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: Plain text
#: debian-bullseye
msgid ""
"If an optional argument is present, then B<swaplabel> will change the "
"appropriate value on I<device>.  These values can also be set during swap "
"creation using B<mkswap>(8).  The B<swaplabel> utility allows changing the "
"label or UUID on an actively used swap device."
msgstr ""
"Якщо вказано необов'язковий параметр, B<swaplabel> змінить відповідне "
"значення на пристрої I<пристрій>. Ці значення також може бути встановлено "
"під час створення резервної пам'яті на диску за допомогою програми "
"B<mkswap>(8). Допоміжна програма B<swaplabel> надає змогу змінювати мітку "
"або UUID на активному використаному пристрої резервної пам'яті."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>,B< --help>"
msgstr "B<-h>,B< --help>"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-L>,B< --label >I<label>"
msgstr "B<-L>,B< --label >I<мітка>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Specify a new I<label> for the device.  Swap partition labels can be at most "
"16 characters long.  If I<label> is longer than 16 characters, B<swaplabel> "
"will truncate it and print a warning message."
msgstr ""
"Вказати нову I<мітку> пристрою. Мітки розділів резервної пам'яті на диску не "
"повинна перевищувати у довжину 16 символів. Якщо довжина I<мітки> "
"перевищуватиме 16 символів, B<swaplabel> обріже її і виведе попередження."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-U>,B< --uuid >I<UUID>"
msgstr "B<-U>,B< --uuid >I<UUID>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Specify a new I<UUID> for the device.  The I< UUID> must be in the standard "
"8-4-4-4-12 character format, such as is output by B<uuidgen>(1)."
msgstr ""
"Вказати новий I<UUID> для пристрою. I<UUID> має бути вказано у стандартному "
"форматі — 8-4-4-4-12 символів, такому самому, у якому виводить дані "
"B<uuidgen>(1)."

#. type: Plain text
#: debian-bullseye
msgid ""
"B<swaplabel> was written by Jason Borden E<lt>jborden@bluehost.comE<gt> and "
"Karel Zak E<lt>kzak@redhat.comE<gt>."
msgstr ""
"B<swaplabel> було створено Jason Borden E<lt>jborden@bluehost.comE<gt> і "
"Karel Zak E<lt>kzak@redhat.comE<gt>."

#. type: Plain text
#: debian-bullseye
msgid ""
"The swaplabel command is part of the util-linux package and is available "
"from https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""
"Програма swaplabel є частиною пакунка util-linux і доступна за адресою "
"https://www.kernel.org/pub/linux/utils/util-linux/."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2021-06-02"
msgstr "2 червня 2021 року"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
