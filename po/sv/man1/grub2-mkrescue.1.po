# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 18:50+0200\n"
"PO-Revision-Date: 2022-07-22 22:17+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKRESCUE"
msgstr "GRUB-MKRESCUE"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "August 2022"
msgstr "augusti 2022"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "grub-mkrescue - make a GRUB rescue image"
msgstr ""

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"B<grub-mkrescue> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>] I<\\,SOURCE\\/>..."
msgstr ""
"B<grub-mkrescue> [I<\\,FLAGGA\\/>...] [I<\\,FLAGGA\\/>] I<\\,KÄLLA\\/>..."

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Make GRUB CD-ROM, disk, pendrive and floppy bootable image."
msgstr "Skapa startbar GRUB-avbild för cd-rom, disk, usb-sticka och diskett."

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<--appended-signature-size>=I<\\,STORLEK\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--compress>=I<\\,no\\/>|xz|gz|lzo"
msgstr "B<--compress>=I<\\,no\\/>|xz|gz|lzo"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "compress GRUB files [optional]"
msgstr "komprimera GRUB-filer [valfritt]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr "inaktivera shim_lock verifier"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--dtb>=I<\\,FILE\\/>"
msgstr "B<--dtb>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "embed a specific DTB"
msgstr "bädda in en specifik DTB"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,KATALOG\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"använd avbilder och moduler under KAT [standard=/usr/lib/grub/"
"E<lt>platformE<gt>]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--fonts>=I<\\,FONTS\\/>"
msgstr "B<--fonts>=I<\\,TYPSNITT\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "install FONTS [default=unicode]"
msgstr "installera TYPSNITT [standard=unicode]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--install-modules>=I<\\,MODULES\\/>"
msgstr "B<--install-modules>=I<\\,MODULER\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "install only MODULES and their dependencies [default=all]"
msgstr "installera endast MODULER och deras beroenden [standard=all]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "embed FILE as public key for signature checking"
msgstr "bädda in FIL som öppen nyckel för signaturkontroll"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locale-directory>=I<\\,DIR\\/> use translations under DIR"
msgstr "B<--locale-directory>=I<\\,DIR\\/> använd översättningar i KAT"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "[default=/usr/share/locale]"
msgstr "[standard=/usr/share/locale]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--locales>=I<\\,LOCALES\\/>"
msgstr "B<--locales>=I<\\,SPRÅK\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "install only LOCALES [default=all]"
msgstr "installera endast SPRÅK [standard=all]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--modules>=I<\\,MODULES\\/>"
msgstr "B<--modules>=I<\\,MODULER\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "pre-load specified modules MODULES"
msgstr "förinläs specificerade moduler MODULER"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--sbat>=I<\\,FILE\\/>"
msgstr "B<--sbat>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr "SBAT-metadata"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--themes>=I<\\,THEMES\\/>"
msgstr "B<--themes>=I<\\,TEMAN\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "install THEMES [default=starfield]"
msgstr "installera TEMAN [standard=starfield]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "skriv ut informativa meddelanden."

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509key>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509key>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "embed FILE as public key for signature checking"
msgid "embed FILE as an x509 certificate for signature checking"
msgstr "bädda in FIL som öppen nyckel för signaturkontroll"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--arcs-boot>"
msgstr "B<--arcs-boot>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"enable ARCS (big-endian mips machines, mostly SGI) boot. Disables HFS+, APM, "
"sparc64 and boot as disk image for i386-pc"
msgstr ""
"aktiverar ARCS-start (big-endian mips-maskiner, mest SGI). Inaktiverar HFS+, "
"APM, sparc64 och att starta som en diskavbild för i386-pc"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--core-compress>=I<\\,xz\\/>|none|auto"
msgstr "B<--core-compress>=I<\\,xz\\/>|none|auto"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "välj komprimering att använda för kärnavbild"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--label-bgcolor>=I<\\,COLOR\\/>"
msgstr "B<--label-bgcolor>=I<\\,FÄRG\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "use COLOR for label background"
msgstr "använd FÄRG för etikettbakgrund"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--label-color>=I<\\,COLOR\\/>"
msgstr "B<--label-color>=I<\\,FÄRG\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "use COLOR for label"
msgstr "använd FÄRG för etikett"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--label-font>=I<\\,FILE\\/>"
msgstr "B<--label-font>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "use FILE as font for label"
msgstr "använd FIL som typsnitt för etikett"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "save output in FILE [required]"
msgstr "spara utmatning i FIL [krävs]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--product-name>=I<\\,STRING\\/>"
msgstr "B<--product-name>=I<\\,STRÄNG\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "use STRING as product name"
msgstr "använd STRÄNG som produktnamn"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--product-version>=I<\\,STRING\\/>"
msgstr "B<--product-version>=I<\\,STRÄNG\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "use STRING as product version"
msgstr "använd STRÄNG som produktversion"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--rom-directory>=I<\\,DIR\\/>"
msgstr "B<--rom-directory>=I<\\,KATALOG\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "save ROM images in DIR [optional]"
msgstr "spara ROM-avbilder i KATALOG [valfritt]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--sparc-boot>"
msgstr "B<--sparc-boot>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"enable sparc boot. Disables HFS+, APM, ARCS and boot as disk image for i386-"
"pc"
msgstr ""
"aktiverar sparc-start. Inaktiverar HFS+, APM, ARCS och starta som diskavbild "
"för i386-pc"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--xorriso>=I<\\,FILE\\/>"
msgstr "B<--xorriso>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "use FILE as xorriso [optional]"
msgstr "använd FIL som xorriso [valfritt]"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "visa denna hjälplista"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "ge ett kort användningsmeddelande"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "print program version"
msgstr "skriv ut programversion"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriska eller valfria argument till långa flaggor är också "
"obligatoriska eller valfria för motsvarande korta flaggor."

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"Generates a bootable CD/USB/floppy image.  Arguments other than options to "
"this program are passed to xorriso, and indicate source files, source "
"directories, or any of the mkisofs options listed by the output of `xorriso "
"B<-as> mkisofs B<-help>'."
msgstr ""
"Genererar en startbar cd-/usb-/diskettavbild.  Andra argument än flaggorna "
"till detta program skickas till xorriso, och indikerar källfiler, "
"källkataloger eller andra mkisofs-alternativ listade av utmatningen av "
"”xorriso B<-as> mkisofs B<-help>”."

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Option B<--> switches to native xorriso command mode."
msgstr "Flaggan B<--> växlar till inbyggt xorriso-kommandoläge."

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Mail xorriso support requests to E<lt>bug-xorriso@gnu.orgE<gt>."
msgstr "E-posta xorriso E<lt>bug-xorriso@gnu.orgE<gt> för stöd."

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OCKSÅ"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<grub-mkimage>(1)"
msgstr "B<grub-mkimage>(1)"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkrescue> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkrescue> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-mkrescue> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-mkrescue> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<info grub-mkrescue>"
msgstr "B<info grub-mkrescue>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "April 2022"
msgstr "april 2022"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "grub-mkrescue (GRUB2) 2.06"
msgstr "grub-mkrescue (GRUB2) 2.06"

#. type: Plain text
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"använd avbilder och moduler under KAT [standard=/usr/share/grub2/"
"E<lt>platformE<gt>]"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "June 2022"
msgstr "juni 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.06"
msgstr "GRUB2 2.06"
