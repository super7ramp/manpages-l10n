# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-05-26 15:06+0200\n"
"PO-Revision-Date: 1999-04-12 19:53+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "MPOOL"
msgstr "MPOOL"

#. type: TH
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 Marzo 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "mpool - shared memory buffer pool"
msgstr "mpool - depósito de buffers de memoria compartida"

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>db.hE<gt>\n"
#| "#include E<lt>mpool.hE<gt>>\n"
msgid ""
"B<#include E<lt>db.hE<gt>>\n"
"B<#include E<lt>mpool.hE<gt>>\n"
msgstr ""
"B<#include E<lt>db.hE<gt>\n"
"#include E<lt>mpool.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<MPOOL *\n"
#| "mpool_open (DBT *key, int fd, pgno_t pagesize, pgno_t maxcache);>\n"
msgid "B<MPOOL *mpool_open(DBT *>I<key>B<, int >I<fd>B<, pgno_t >I<pagesize>B<, pgno_t >I<maxcache>B<);>\n"
msgstr ""
"B<MPOOL *\n"
"mpool_open (DBT *key, int fd, pgno_t pagesize, pgno_t maxcache);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void mpool_filter(MPOOL *>I<mp>B<, void (*pgin)(void *, pgno_t, void *),>\n"
"B<                  void (*>I<pgout>B<)(void *, pgno_t, void *),>\n"
"B<                  void *>I<pgcookie>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *mpool_new(MPOOL *>I<mp>B<, pgno_t *>I<pgnoaddr>B<);>\n"
"B<void *mpool_get(MPOOL *>I<mp>B<, pgno_t >I<pgno>B<, unsigned int >I<flags>B<);>\n"
"B<int mpool_put(MPOOL *>I<mp>B<, void *>I<pgaddr>B<, unsigned int >I<flags>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int\n"
#| "mpool_close (MPOOL *mp);>\n"
msgid ""
"B<int mpool_sync(MPOOL *>I<mp>B<);>\n"
"B<int mpool_close(MPOOL *>I<mp>B<);>\n"
msgstr ""
"B<int\n"
"mpool_close (MPOOL *mp);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"I<Note well>: This page documents interfaces provided in glibc up until "
"version 2.1.  Since version 2.2, glibc no longer provides these interfaces.  "
"Probably, you are looking for the APIs provided by the I<libdb> library "
"instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"I<Mpool> is the library interface intended to provide page oriented buffer "
"management of files.  The buffers may be shared between processes."
msgstr ""
"I<Mpool> es la interfaz de biblioteca destinada a proporcionar un manejo de "
"buffers de fichero orientado a páginas.  Los buffers pueden ser compartidos "
"entre procesos."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function I<mpool_open> initializes a memory pool.  The I<key> "
#| "argument is the byte string used to negotiate between multiple processes "
#| "wishing to share buffers.  If the file buffers are mapped in shared "
#| "memory, all processes using the same key will share the buffers.  If "
#| "I<key> is NULL, the buffers are mapped into private memory.  The I<fd> "
#| "argument is a file descriptor for the underlying file, which must be "
#| "seekable.  If I<key> is non-NULL and matches a file already being mapped, "
#| "the I<fd> argument is ignored."
msgid ""
"The function B<mpool_open>()  initializes a memory pool.  The I<key> "
"argument is the byte string used to negotiate between multiple processes "
"wishing to share buffers.  If the file buffers are mapped in shared memory, "
"all processes using the same key will share the buffers.  If I<key> is NULL, "
"the buffers are mapped into private memory.  The I<fd> argument is a file "
"descriptor for the underlying file, which must be seekable.  If I<key> is "
"non-NULL and matches a file already being mapped, the I<fd> argument is "
"ignored."
msgstr ""
"La función I<mpool_open> inicializa un depósito de memoria.  El argumento "
"I<key> es la cadena de bytes usada para negociar entre varios procesos que "
"desean compartir buffers.  Si los buffers de fichero se asocian a memoria "
"compartida, todos los procesos que usen la misma clave compartirán los "
"buffers.  Si I<key> es NULL, los buffers se asocian a una memoria privada.  "
"El argumento I<fd> es un descriptor de fichero para el fichero subyacente, "
"que debe soportar el posicionamiento del puntero de lectura/escritura (es "
"decir, las búsquedas).  Si I<key> no es NULL y coincide con un fichero que "
"ya está asociado, el argumento I<fd> se ignorará."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The I<pagesize> argument is the size, in bytes, of the pages into which the "
"file is broken up.  The I<maxcache> argument is the maximum number of pages "
"from the underlying file to cache at any one time.  This value is not "
"relative to the number of processes which share a file's buffers, but will "
"be the largest value specified by any of the processes sharing the file."
msgstr ""
"El argumento I<pagesize> es el tamaño, en bytes, de las páginas en las que "
"se descompone el fichero.  El argumento I<maxcache> es el número máximo de "
"páginas del fichero subyacente a colocar en cache en todo momento.  Esta "
"valor no es relativo al número de procesos que comparten los buffers de un "
"fichero, pero será el mayor valor especificado por cualquiera de los "
"procesos que compartan el fichero."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_filter> function is intended to make transparent input and "
#| "output processing of the pages possible.  If the I<pgin> function is "
#| "specified, it is called each time a buffer is read into the memory pool "
#| "from the backing file.  If the I<pgout> function is specified, it is "
#| "called each time a buffer is written into the backing file.  Both "
#| "functions are are called with the I<pgcookie> pointer, the page number "
#| "and a pointer to the page to being read or written."
msgid ""
"The B<mpool_filter>()  function is intended to make transparent input and "
"output processing of the pages possible.  If the I<pgin> function is "
"specified, it is called each time a buffer is read into the memory pool from "
"the backing file.  If the I<pgout> function is specified, it is called each "
"time a buffer is written into the backing file.  Both functions are called "
"with the I<pgcookie> pointer, the page number and a pointer to the page to "
"being read or written."
msgstr ""
"La función I<mpool_filter> está destinada a hacer transparente el "
"procesamiento de la entrada y la salida de las posibles páginas.  Si se "
"especifica la función I<pgin>, se llamará cada vez que se lea un buffer al "
"depósito de memoria procedente del fichero de respaldo.  Si se especifica la "
"función I<pgout>, se llamará cada vez que un buffer se escriba en el fichero "
"de respaldo.  Ambas funciones se llaman con el puntero I<pgcookie>, el "
"número de página y un puntero a la página a leer o escribir."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function I<mpool_new> takes an MPOOL pointer and an address as "
#| "arguments.  If a new page can be allocated, a pointer to the page is "
#| "returned and the page number is stored into the I<pgnoaddr> address.  "
#| "Otherwise, NULL is returned and errno is set."
msgid ""
"The function B<mpool_new>()  takes an I<MPOOL> pointer and an address as "
"arguments.  If a new page can be allocated, a pointer to the page is "
"returned and the page number is stored into the I<pgnoaddr> address.  "
"Otherwise, NULL is returned and I<errno> is set."
msgstr ""
"La función I<mpool_new> toma un puntero MPOOL y una dirección como "
"argumentos.  Si se puede asignar una nueva página, se devolverá un puntero a "
"la página y el número de página se almacenará en la dirección I<pgnoaddr>.  "
"En caso contrario, se devolverá NULL y se asignará un valor a errno."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function I<mpool_get> takes a MPOOL pointer and a page number as "
#| "arguments.  If the page exists, a pointer to the page is returned.  "
#| "Otherwise, NULL is returned and errno is set.  The flags parameter is not "
#| "currently used."
msgid ""
"The function B<mpool_get>()  takes an I<MPOOL> pointer and a page number as "
"arguments.  If the page exists, a pointer to the page is returned.  "
"Otherwise, NULL is returned and I<errno> is set.  The I<flags> argument is "
"not currently used."
msgstr ""
"La función I<mpool_get> toma un puntero MPOOL y un número de página como "
"argumentos.  Si la página existe, devolverá un puntero a la página.  En caso "
"contrario, devolverá NULL y se asignará un valor a errno.  El parámetro de "
"opción no se usa actualmente."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function I<mpool_put> unpins the page referenced by I<pgaddr>.  "
#| "I<Pgaddr> must be an address previously returned by I<mpool_get> or "
#| "I<mpool_new>.  The flag value is specified by I<or>'ing any of the "
#| "following values:"
msgid ""
"The function B<mpool_put>()  unpins the page referenced by I<pgaddr>.  "
"I<pgaddr> must be an address previously returned by B<mpool_get>()  or "
"B<mpool_new>().  The flag value is specified by ORing any of the following "
"values:"
msgstr ""
"La función I<mpool_put> desprende la página referenciada por I<pgaddr>.  "
"I<Pgaddr> debe ser una dirección devuelta previamente por I<mpool_get> o "
"I<mpool_new>.  El valor de opción se especifica haciendo una operación I<O>-"
"lógica con cualquiera de los siguientes valores:"

#. type: TP
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MPOOL_DIRTY"
msgid "B<MPOOL_DIRTY>"
msgstr "MPOOL_DIRTY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "The page has been modified and needs to be written to the backing file."
msgstr ""
"La página ha sido modificada y necesita ser escrita en el fichero de "
"respaldo."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "I<Mpool_put> returns 0 on success and -1 if an error occurs."
msgid "B<mpool_put>()  returns 0 on success and -1 if an error occurs."
msgstr "I<Mpool_put> devuelve 0 en caso de éxito y -1 si se produce un error."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The function I<mpool_sync> writes all modified pages associated with the "
#| "MPOOL pointer to the backing file.  I<Mpool_sync> returns 0 on success "
#| "and -1 if an error occurs."
msgid ""
"The function B<mpool_sync>()  writes all modified pages associated with the "
"I<MPOOL> pointer to the backing file.  B<mpool_sync>()  returns 0 on success "
"and -1 if an error occurs."
msgstr ""
"La función I<mpool_sync> escribe en el fichero de respaldo todas las páginas "
"modificadas asociadas con el puntero MPOOL.  I<Mpool_sync> devuelve 0 en "
"caso de éxito y -1 si se produce un error."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_close> function free's up any allocated memory associated "
#| "with the memory pool cookie.  Modified pages are B<not> written to the "
#| "backing file.  I<Mpool_close> returns 0 on success and -1 if an error "
#| "occurs."
msgid ""
"The B<mpool_close>()  function free's up any allocated memory associated "
"with the memory pool cookie.  Modified pages are B<not> written to the "
"backing file.  B<mpool_close>()  returns 0 on success and -1 if an error "
"occurs."
msgstr ""
"La función I<mpool_close> libera cualquier memoria reservada asociada con el "
"depósito de memoria.  Las páginas modificadas B<no> se escribirán en el "
"fichero de respaldo.  I<Mpool_close> devuelve 0 en caso de éxito y -1 si se "
"produce un error."

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_open> function may fail and set I<errno> for any of the "
#| "errors specified for the library routine I<malloc>(3)."
msgid ""
"The B<mpool_open>()  function may fail and set I<errno> for any of the "
"errors specified for the library routine B<malloc>(3)."
msgstr ""
"La función I<mpool_open> puede fallar y asignar a I<errno> cualquiera de los "
"errores especificados para la rutina de biblioteca I<malloc>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_get> function may fail and set I<errno> for the following:"
msgid ""
"The B<mpool_get>()  function may fail and set I<errno> for the following:"
msgstr ""
"La función I<mpool_get> puede fallar y asignar a I<errno> uno de los "
"siguiente valores:"

#. type: TP
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "The requested record doesn't exist."
msgstr "El registro solicitado no exite."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_new> and I<mpool_get> functions may fail and set I<errno> for "
#| "any of the errors specified for the library routines I<read>(2)I<,> "
#| "I<write>(2)I<,> and I<malloc>(3)."
msgid ""
"The B<mpool_new>()  and B<mpool_get>()  functions may fail and set I<errno> "
"for any of the errors specified for the library routines B<read>(2), "
"B<write>(2), and B<malloc>(3)."
msgstr ""
"Las funciones I<mpool_new> y I<mpool_get> pueden fallar y asignar a I<errno> "
"cualquiera de los errores especificados para las rutinas de biblioteca "
"I<read>(2)I<,> I<write>(2)  y I<malloc>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_sync> function may fail and set I<errno> for any of the "
#| "errors specified for the library routine I<write>(2)."
msgid ""
"The B<mpool_sync>()  function may fail and set I<errno> for any of the "
"errors specified for the library routine B<write>(2)."
msgstr ""
"La función I<mpool_sync> puede fallar y asignar a I<errno> cualquiera de los "
"errores especificados para la rutina de biblioteca I<write>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<mpool_close> function may fail and set I<errno> for any of the "
#| "errors specified for the library routine I<free>(3)."
msgid ""
"The B<mpool_close>()  function may fail and set I<errno> for any of the "
"errors specified for the library routine B<free>(3)."
msgstr ""
"La función I<mpool_close> puede fallar y asignar a I<errno> cualquiera de "
"los errores especificados para la rutina de biblioteca I<free>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Not in POSIX.1.  Present on the BSDs."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "I<dbopen>(3), I<btree>(3), I<hash>(3), I<recno>(3)"
msgid "B<btree>(3), B<dbopen>(3), B<hash>(3), B<recno>(3)"
msgstr "I<dbopen>(3), I<btree>(3), I<hash>(3), I<recno>(3)"

#. type: SH
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: archlinux debian-unstable mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.13 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy, no-wrap
#| msgid ""
#| "B<void *\n"
#| "mpool_new (MPOOL *mp, pgno_t *pgnoaddr);>\n"
msgid "B<void *mpool_new(MPOOL *>I<mp>B<, pgno_t *>I<pgnoaddr>B<);>\n"
msgstr ""
"B<void *\n"
"mpool_new (MPOOL *mp, pgno_t *pgnoaddr);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy, no-wrap
#| msgid ""
#| "B<void *\n"
#| "mpool_get (MPOOL *mp, pgno_t pgno, u_int flags);>\n"
msgid "B<void *mpool_get(MPOOL *>I<mp>B<, pgno_t >I<pgno>B<, unsigned int >I<flags>B<);>\n"
msgstr ""
"B<void *\n"
"mpool_get (MPOOL *mp, pgno_t pgno, u_int flags);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy, no-wrap
#| msgid ""
#| "B<int\n"
#| "mpool_put (MPOOL *mp, void *pgaddr, u_int flags);>\n"
msgid "B<int mpool_put(MPOOL *>I<mp>B<, void *>I<pgaddr>B<, unsigned int >I<flags>B<);>\n"
msgstr ""
"B<int\n"
"mpool_put (MPOOL *mp, void *pgaddr, u_int flags);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy, no-wrap
#| msgid ""
#| "B<int\n"
#| "mpool_sync (MPOOL *mp);>\n"
msgid "B<int mpool_sync(MPOOL *>I<mp>B<);>\n"
msgstr ""
"B<int\n"
"mpool_sync (MPOOL *mp);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy, no-wrap
#| msgid ""
#| "B<int\n"
#| "mpool_close (MPOOL *mp);>\n"
msgid "B<int mpool_close(MPOOL *>I<mp>B<);>\n"
msgstr ""
"B<int\n"
"mpool_close (MPOOL *mp);>\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
