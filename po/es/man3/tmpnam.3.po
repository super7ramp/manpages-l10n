# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 19:16+0200\n"
"PO-Revision-Date: 1999-06-27 19:55+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "TMPNAM"
msgstr "TMPNAM"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 Marzo 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "tmpnam, tmpnam_r - create a name for a temporary file"
msgstr "tmpnam, tmpnam_r - crea un nombre para un fichero temporal"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *tmpnam(char *>I<s>B<);>\n"
"B<char *tmpnam_r(char *>I<s>B<);>\n"
msgstr ""
"B<char *tmpnam(char *>I<s>B<);>\n"
"B<char *tmpnam_r(char *>I<s>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpnam_r>()"
msgstr "B<tmpnam_r>()"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Up to and including glibc 2.19:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "Never use this function. Use B<mkstemp>(3)  instead."
msgid ""
"B<Note:> avoid using these functions; use B<mkstemp>(3)  or B<tmpfile>(3)  "
"instead."
msgstr "Nunca use esta función. En su lugar use B<mkstemp>(3)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<tmpnam>()  function returns a pointer to a string that is a valid "
"filename, and such that a file with this name did not exist at some point in "
"time, so that naive programmers may think it a suitable name for a temporary "
"file.  If the argument I<s> is NULL, this name is generated in an internal "
"static buffer and may be overwritten by the next call to B<tmpnam>().  If "
"I<s> is not NULL, the name is copied to the character array (of length at "
"least I<L_tmpnam>)  pointed to by I<s> and the value I<s> is returned in "
"case of success."
msgstr ""
"La función B<tmpnam>() devuelve un puntero a una cadena que es un nombre "
"válido de fichero tal que no existe un fichero con ese nombre en ningún "
"instante por lo que los programadores ingénuos pueden pensar en él como en "
"un nombre adecuado para un fichero temporal. Si el argumento I<s> es NULL "
"este nombre se genera en un área estática interna que puede ser sobreescrito "
"por la siguiente llamada a B<tmpnam>().  Si I<s> no es NULL, el nombre se "
"copia al array de caracteres (de longitud, al menos, I<L_tmpnam>)  apuntado "
"por I<s> y se devuelve el valor I<s> en caso de éxito."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The created pathname has a directory prefix I<P_tmpdir>.  (Both I<L_tmpnam> "
"and I<P_tmpdir> are defined in I<E<lt>stdio.hE<gt>>, just like the "
"B<TMP_MAX> mentioned below.)"
msgstr ""
"La ruta que se crea tiene como prefijo de directorios I<P_tmpdir>.  (Tanto "
"I<L_tmpnam> como I<P_tmpdir> se definen en I<E<lt>stdio.hE<gt>>, de la misma "
"manera que el valor B<TMP_MAX> mencionado más abajo)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<tmpnam_r>()  function performs the same task as B<tmpnam>(), but "
"returns NULL (to indicate an error) if I<s> is NULL."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<tmpnam()> function returns a pointer to a unique temporary "
#| "filename, or NULL if a unique name cannot be generated."
msgid ""
"These functions return a pointer to a unique temporary filename, or NULL if "
"a unique name cannot be generated."
msgstr ""
"La función B<tmpnam()> devuelve un puntero al nombre único de fichero "
"temporal, o NULL si no se puede generar un nombre único."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "No errors are defined."
msgstr "No se han definido errores."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpnam>()"
msgstr "B<tmpnam>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:tmpnam/!s"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<tmpnam>(): SVr4, 4.3BSD, C89, C99, POSIX.1-2001.  POSIX.1-2008 marks "
"B<tmpnam>()  as obsolete."
msgstr ""

#.  Appears to be on Solaris
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<tmpnam_r>()  is a nonstandard extension that is also available on a few "
"other systems."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<tmpnam>()  function generates a different string each time it is "
"called, up to B<TMP_MAX> times.  If it is called more than B<TMP_MAX> times, "
"the behavior is implementation defined."
msgstr ""
"La función B<tmpnam>() genera una cadena diferente cada vez que se llama, "
"hasta B<TMP_MAX> veces. Si se llama más de B<TMP_MAX> veces, el "
"comportamiento depende de la implementación."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Although these functions generate names that are difficult to guess, it is "
"nevertheless possible that between the time that the pathname is returned "
"and the time that the program opens it, another program might create that "
"pathname using B<open>(2), or create it as a symbolic link.  This can lead "
"to security holes.  To avoid such possibilities, use the B<open>(2)  "
"B<O_EXCL> flag to open the pathname.  Or better yet, use B<mkstemp>(3)  or "
"B<tmpfile>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Portable applications that use threads cannot call B<tmpnam()> with NULL "
#| "parameter if either _POSIX_THREAD_SAFE_FUNCTIONS or _POSIX_THREADS is "
#| "defined."
msgid ""
"Portable applications that use threads cannot call B<tmpnam>()  with a NULL "
"argument if either B<_POSIX_THREADS> or B<_POSIX_THREAD_SAFE_FUNCTIONS> is "
"defined."
msgstr ""
"Las aplicaciones transportables que usan hilos no pueden llamar a "
"B<tmpnam()> con un parámetro NULL si se define o bien "
"_POSIX_THREAD_SAFE_FUNCTIONS o bien _POSIX_THREADS."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "Never use this function. Use B<mkstemp>(3)  instead."
msgid ""
"Never use these functions.  Use B<mkstemp>(3)  or B<tmpfile>(3)  instead."
msgstr "Nunca use esta función. En su lugar use B<mkstemp>(3)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mkstemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3)"
msgstr "B<mkstemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.13 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TP
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "Since glibc 2.19:"
msgstr "Desde glibc 2.19:"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "_DEFAULT_SOURCE"
msgstr "_DEFAULT_SOURCE"

#. type: TP
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "Up to and including glibc 2.19:"
msgstr ""

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "_BSD_SOURCE || _SVID_SOURCE"
msgstr "_BSD_SOURCE || _SVID_SOURCE"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
