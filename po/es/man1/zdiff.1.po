# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Vicente Pastor Gómez <vpastorg@santandersupernet.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 19:20+0200\n"
"PO-Revision-Date: 1998-06-12 19:53+0200\n"
"Last-Translator: Vicente Pastor Gómez <vpastorg@santandersupernet.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ZDIFF"
msgstr "ZDIFF"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "zcmp, zdiff - compare compressed files"
msgstr "zcmp, zdiff - comparar ficheros comprimidos"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<zcmp> [ cmp_options ] file1 [ file2 ]"
msgstr "B<zcmp> [ cmp_opciones ] fichero1 [ fichero2 ]"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<zdiff> [ diff_options ] file1 [ file2 ]"
msgstr "B<zdiff> [ diff_opciones ] fichero1 [ fichero2 ]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Zcmp> and I<zdiff> are used to invoke the I<cmp> or the I<diff> program "
#| "on compressed files.  All options specified are passed directly to I<cmp> "
#| "or I<diff>.  If only 1 file is specified, then the files compared are "
#| "I<file1> and an uncompressed I<file1>.gz.  If two files are specified, "
#| "then they are uncompressed if necessary and fed to I<cmp> or I<diff>.  "
#| "The exit status from I<cmp> or I<diff> is preserved."
msgid ""
"The B<zcmp> and B<zdiff> commands are used to invoke the B<cmp> or the "
"B<diff> program on files compressed via B<gzip>.  All options specified are "
"passed directly to B<cmp> or B<diff>.  If only I<file1> is specified, it is "
"compared to the uncompressed contents of I<file1>B<.gz>I<.> If two files are "
"specified, their contents (uncompressed if necessary) are fed to B<cmp> or "
"B<diff>.  The input files are not modified.  The exit status from B<cmp> or "
"B<diff> is preserved."
msgstr ""
"I<Zcmp> y I<zdiff> se usan para invocar los programas I<cmp> o I<diff> sobre "
"ficheros comprimidos. Todas las opciones especificadas se pasan directamente "
"a I<cmp> o I<diff>.  Si solo se especifica 1 fichero, entonces los ficheros "
"comparados son I<fichero1> y una copia descomprimida de I<fichero1>.gz.  Si "
"se especifican 2 ficheros, entonces son descomprimidos si es necesario, y "
"pasados a I<cmp> o I<diff>.  El estado de salida de I<cmp> o I<diff> es "
"preservado."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"cmp(1), diff(1), zmore(1), zgrep(1), znew(1), zforce(1), gzip(1), gzexe(1)"
msgstr ""
"cmp(1), diff(1), zmore(1), zgrep(1), znew(1), zforce(1), gzip(1), gzexe(1)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Messages from the I<cmp> or I<diff> programs refer to temporary filenames "
#| "instead of those specified."
msgid ""
"Messages from the B<cmp> or B<diff> programs may refer to file names such as "
"\"-\" instead of to the file names specified."
msgstr ""
"Los mensajes de los programas I<cmp> o I<diff> se refieren a nombres de "
"fichero temporales, en vez de a los especificados."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy
#| msgid ""
#| "I<Zcmp> and I<zdiff> are used to invoke the I<cmp> or the I<diff> program "
#| "on compressed files.  All options specified are passed directly to I<cmp> "
#| "or I<diff>.  If only 1 file is specified, then the files compared are "
#| "I<file1> and an uncompressed I<file1>.gz.  If two files are specified, "
#| "then they are uncompressed if necessary and fed to I<cmp> or I<diff>.  "
#| "The exit status from I<cmp> or I<diff> is preserved."
msgid ""
"I<Zcmp> and I<zdiff> are used to invoke the I<cmp> or the I<diff> program on "
"files compressed via I<gzip>.  All options specified are passed directly to "
"I<cmp> or I<diff>.  If only I<file1> is specified, it is compared to the "
"uncompressed contents of I<file1>.gz.  If two files are specified, their "
"contents (uncompressed if necessary) are fed to I<cmp> or I<diff>.  The "
"input files are not modified.  The exit status from I<cmp> or I<diff> is "
"preserved."
msgstr ""
"I<Zcmp> y I<zdiff> se usan para invocar los programas I<cmp> o I<diff> sobre "
"ficheros comprimidos. Todas las opciones especificadas se pasan directamente "
"a I<cmp> o I<diff>.  Si solo se especifica 1 fichero, entonces los ficheros "
"comparados son I<fichero1> y una copia descomprimida de I<fichero1>.gz.  Si "
"se especifican 2 ficheros, entonces son descomprimidos si es necesario, y "
"pasados a I<cmp> o I<diff>.  El estado de salida de I<cmp> o I<diff> es "
"preservado."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy
#| msgid ""
#| "Messages from the I<cmp> or I<diff> programs refer to temporary filenames "
#| "instead of those specified."
msgid ""
"Messages from the I<cmp> or I<diff> programs may refer to file names such as "
"\"-\" instead of to the file names specified."
msgstr ""
"Los mensajes de los programas I<cmp> o I<diff> se refieren a nombres de "
"fichero temporales, en vez de a los especificados."
