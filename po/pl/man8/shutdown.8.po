# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Wojtek Kotwica <wkotwica@post.pl>, 1999.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2022-08-19 19:09+0200\n"
"PO-Revision-Date: 2022-01-26 18:12+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SHUTDOWN"
msgstr "SHUTDOWN"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 251"
msgstr "systemd 251"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "shutdown"
msgstr "shutdown"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "shutdown - Halt, power-off or reboot the machine"
msgid "shutdown - Halt, power off or reboot the machine"
msgstr "shutdown - zatrzymuje, wyłącza lub przeładowuje komputer"

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<shutdown> [OPTIONS...] [TIME] [WALL...]"
msgstr "B<shutdown> [I<OPCJE>...] [I<CZAS>] [I<KOMUNIKAT-WALL>...]"

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<shutdown> may be used to halt, power-off or reboot the machine\\&."
msgid "B<shutdown> may be used to halt, power off, or reboot the machine\\&."
msgstr ""
"B<shutdown> służy do zatrzymania, wyłączenia lub przeładowania komputera\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The first argument may be a time string (which is usually \"now\")\\&. "
"Optionally, this may be followed by a wall message to be sent to all logged-"
"in users before going down\\&."
msgstr ""
"Pierwszym argumentem jest łańcuch oznaczający czas (zwykle B<now>)\\&. "
"Opcjonalnie, można po nim podać komunikat wall, który przed wyłączeniem "
"zostanie wysłany do wszystkich zalogowanych użytkowników."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The time string may either be in the format \"hh:mm\" for hour/minutes "
"specifying the time to execute the shutdown at, specified in 24h clock "
"format\\&. Alternatively it may be in the syntax \"+m\" referring to the "
"specified number of minutes m from now\\&.  \"now\" is an alias for \"+0\", "
"i\\&.e\\&. for triggering an immediate shutdown\\&. If no time argument is "
"specified, \"+1\" is implied\\&."
msgstr ""
"Łańcuch oznaczający I<CZAS> może przyjąć postać I<gg>B<:>I<mm> - wyłączenie "
"nastąpi wówczas o podanym czasie, przy czym godzinę należy podać w formacie "
"24 godzinnym\\&. Alternatywą jest składnia B<+>I<m> - polecenie zostanie "
"wykonane po upływie podanej liczby minut\\&. Słowo B<now> jest aliasem "
"B<+0>, tzn. natychmiastowego wyłączenia\\&. Jeśli nie poda się argumentu "
"czasu, przyjmowane jest B<+1>\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Note that to specify a wall message you must specify a time argument, too\\&."
msgstr ""
"Proszę zauważyć, że aby móc podać I<KOMUNIKAT-WALL>, konieczne jest podanie "
"również argumentu oznaczającego czas\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If the time argument is used, 5 minutes before the system goes down the /run/"
"nologin file is created to ensure that further logins shall not be "
"allowed\\&."
msgstr ""
"Jeśli użyje się argumentu czasu, 5 minut przed wyłączeniem tworzony jest "
"plik I</run/nologin>, aby uniemożliwić nowe zalogowania się\\&."

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "The following options are understood:"
msgstr "Obsługiwane są następujące opcje:"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr "Wyświetla krótki komunikat pomocy i wychodzi\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-H>, B<--halt>"
msgstr "B<-H>, B<--halt>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Halt the machine\\&."
msgstr "Zatrzymuje komputer\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-P>, B<--poweroff>"
msgstr "B<-P>, B<--poweroff>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Power-off the machine (the default)\\&."
msgid "Power the machine off (the default)\\&."
msgstr "Wyłącza komputer (domyślnie)\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-r>, B<--reboot>"
msgstr "B<-r>, B<--reboot>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Reboot the machine\\&."
msgstr "Przeładowuje komputer\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The same as B<--poweroff>, but does not override the action to take if it is "
"\"halt\"\\&. E\\&.g\\&.  B<shutdown --reboot -h> means \"poweroff\", but "
"B<shutdown --halt -h> means \"halt\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Do not halt, power-off, reboot, just write wall message\\&."
msgid "Do not halt, power off, or reboot, but just write the wall message\\&."
msgstr ""
"Nie zatrzymuje, wyłącza ani przeładowuje komputera, wysyła jedynie "
"I<KOMUNIKAT-WALL>\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<--no-wall>"
msgstr "B<--no-wall>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid "Do not send wall message before halt, power-off, reboot\\&."
msgid "Do not send wall message before halt, power off, or reboot\\&."
msgstr ""
"Nie wysyła komunikatu wall przed zatrzymaniem, wyłączeniem lub "
"przeładowaniem\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Cancel a pending shutdown\\&. This may be used to cancel the effect of an "
"invocation of B<shutdown> with a time argument that is not \"+0\" or "
"\"now\"\\&."
msgstr ""
"Anuluje trwający proces wyłączania\\&. Opcji można użyć, aby anulować "
"wywołanie B<shutdown> z argumentem I<CZASU> innym niż B<+0> lub B<now>\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--show>"
msgstr "B<--show>"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "Show a pending shutdown action and time if there is any\\&."
msgstr ""
"Pokazuje informacje o oczekującym wyłączeniu (jeśli takie istnieje), wraz z "
"pozostałym czasem\\&."

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "KOD ZAKOŃCZENIA"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""
"W przypadku powodzenia zwracane jest 0, w razie błędu kod jest niezerowy\\&."

#. type: SH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<systemd>(1), B<systemctl>(1), B<halt>(8), B<wall>(1)"
msgstr "B<systemd>(1), B<systemctl>(1), B<halt>(8), B<wall>(1)"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#. type: Plain text
#: opensuse-leap-15-4
msgid "shutdown - Halt, power-off or reboot the machine"
msgstr "shutdown - zatrzymuje, wyłącza lub przeładowuje komputer"

#. type: Plain text
#: opensuse-leap-15-4
msgid "B<shutdown> may be used to halt, power-off or reboot the machine\\&."
msgstr ""
"B<shutdown> służy do zatrzymania, wyłączenia lub przeładowania komputera\\&."

#. type: Plain text
#: opensuse-leap-15-4
msgid "Power-off the machine (the default)\\&."
msgstr "Wyłącza komputer (domyślnie)\\&."

#. type: Plain text
#: opensuse-leap-15-4
msgid "Equivalent to B<--poweroff>, unless B<--halt> is specified\\&."
msgstr "Równoważne B<--poweroff>, chyba że poda się również B<--halt>\\&."

#. type: Plain text
#: opensuse-leap-15-4
msgid "Do not halt, power-off, reboot, just write wall message\\&."
msgstr ""
"Nie zatrzymuje, wyłącza ani przeładowuje komputera, wysyła jedynie "
"I<KOMUNIKAT-WALL>\\&."

#. type: Plain text
#: opensuse-leap-15-4
msgid "Do not send wall message before halt, power-off, reboot\\&."
msgstr ""
"Nie wysyła komunikatu wall przed zatrzymaniem, wyłączeniem lub "
"przeładowaniem\\&."
