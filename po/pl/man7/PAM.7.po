# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 2000.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2022-08-19 19:00+0200\n"
"PO-Revision-Date: 2021-01-05 15:20+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "PAM"
msgstr "PAM"

#. type: TH
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "02/18/2020"
msgid "06/08/2020"
msgstr "18 lutego 2020 r"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Linux-PAM Manual"
msgstr "Podręcznik Linux-PAM"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "PAM, pam - Pluggable Authentication Modules for Linux"
msgstr ""
"PAM, pam - dołączalne moduły uwierzytelniania do Linuksa (ang. Pluggable "
"Authentication Modules)"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This manual is intended to offer a quick introduction to B<Linux-PAM>\\&. "
"For more information the reader is directed to the B<Linux-PAM system "
"administrators\\*(Aq guide>\\&."
msgstr ""
"Podręcznik ten ma na celu danie krótkiego wprowadzenia do B<Linux-PAM>. W "
"celu uzyskania dalszych informacji, czytelnik jest odsyłany do B<Linux-PAM "
"system administrators' guide>."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<Linux-PAM> is a system of libraries that handle the authentication tasks "
"of applications (services) on the system\\&. The library provides a stable "
"general interface (Application Programming Interface - API) that privilege "
"granting programs (such as B<login>(1)  and B<su>(1)) defer to to perform "
"standard authentication tasks\\&."
msgstr ""
"B<Linux-PAM> jest systemem bibliotek, które zajmują się zadaniami "
"uwierzytelniania aplikacji (usług) systemu. Biblioteka daje stabilny i "
"ogólny interfejs (API), któremu podlegają w zadaniach uwierzytelniania "
"programy dające przywileje (takie jak B<login>(1)  i B<su>(1))."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The principal feature of the PAM approach is that the nature of the "
"authentication is dynamically configurable\\&. In other words, the system "
"administrator is free to choose how individual service-providing "
"applications will authenticate users\\&. This dynamic configuration is set "
"by the contents of the single B<Linux-PAM> configuration file /etc/pam\\&."
"conf\\&. Alternatively, the configuration can be set by individual "
"configuration files located in the /etc/pam\\&.d/ directory\\&. The presence "
"of this directory will cause B<Linux-PAM> to I<ignore> /etc/pam\\&.conf\\&."
msgstr ""
"Podstawową właściwością podejścia PAM jest to, że natura uwierzytelniania "
"jest dynamicznie konfigurowalna. Innymi słowy, administrator systemu ma "
"pełne pole do popisu w wybieraniu sposobu uwierzytelniania poszczególnych "
"aplikacji.  Ta dynamiczna konfiguracja jest ustawiana zawartością "
"pojedynczego pliku konfiguracyjnego B<Linux-PAM> czyli B</etc/pam.conf>.  "
"Alternatywnie, można wszystko konfigurować pojedynczymi plikami "
"konfiguracyjnymi, zlokalizowanymi w katalogu B</etc/pam.d/>. Obecność tego "
"katalogu spowoduje, że B<Linux-PAM> I<zignoruje> /etc/pam.conf."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Vendor-supplied PAM configuration files might be installed in the system "
"directory /usr/lib/pam\\&.d/ or a configurable vendor specific directory "
"instead of the machine configuration directory /etc/pam\\&.d/\\&. If no "
"machine configuration file is found, the vendor-supplied file is used\\&. "
"All files in /etc/pam\\&.d/ override files with the same name in other "
"directories\\&."
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"From the point of view of the system administrator, for whom this manual is "
"provided, it is not of primary importance to understand the internal "
"behavior of the B<Linux-PAM> library\\&. The important point to recognize is "
"that the configuration file(s)  I<define> the connection between "
"applications (B<services>) and the pluggable authentication modules "
"(B<PAM>s) that perform the actual authentication tasks\\&."
msgstr ""
"Z punktu widzenia administratora systemu, dla którego przeznaczony jest ten "
"podręcznik, nie jest ważne zrozumienie wewnętrznego działania biblioteki "
"B<Linux-PAM>.  Ważną rzeczą jest natomiast rozumienie, że plik(i) "
"konfiguracyjne I<definiują> połączenia między aplikacjami (B<usługami>)  a "
"dołączalnymi modułami uwierzytelniania (B<PAM>-ami), które dokonują "
"rzeczywistych zadań uwierzytelniania."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<Linux-PAM> separates the tasks of I<authentication> into four independent "
"management groups: B<account> management; B<auth>entication management; "
"B<password> management; and B<session> management\\&. (We highlight the "
"abbreviations used for these groups in the configuration file\\&.)"
msgstr ""
"B<Linux-PAM> rozdziela zadania I<uwierzytelniania> na cztery niezależne "
"grupy zarządzania: zarządzanie kontem (B<account>); zarządzanie "
"uwierzytelnianiem (B<auth>entication); zarządzanie hasłami (B<password>); i "
"zarządzanie sesją (B<session>).  (podświetlamy skróty używane dla tych grup "
"w pliku konfiguracyjnym.)"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Simply put, these groups take care of different aspects of a typical "
"user\\*(Aqs request for a restricted service:"
msgstr ""
"Po ustawieniu, grupy te będą się zajmowały różnymi aspektami typowego "
"żądania zastrzeżonej usługi przez użytkownika:"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<account> - provide account verification types of service: has the "
"user\\*(Aqs password expired?; is this user permitted access to the "
"requested service?"
msgstr ""
"B<account> - daj usłudze możliwość weryfikacji konta: czy hasło użytkownika "
"jest przedawnione?; czy użytkownik ma prawo dostępu do żądanej usługi?"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<auth>entication - authenticate a user and set up user credentials\\&. "
"Typically this is via some challenge-response request that the user must "
"satisfy: if you are who you claim to be please enter your password\\&. Not "
"all authentications are of this type, there exist hardware based "
"authentication schemes (such as the use of smart-cards and biometric "
"devices), with suitable modules, these may be substituted seamlessly for "
"more standard approaches to authentication - such is the flexibility of "
"B<Linux-PAM>\\&."
msgstr ""
"B<auth>entication - ustal czy użytkownik jest tym, za którego się podaje. "
"Zazwyczaj robi się to poprzez zapytanie użytkownika o pewną odpowiedź, "
"której musi udzielić: jeśli jesteś tym, za kogo się podajesz, podaj proszę "
"swoje hasło. Nie wszystkie uwierzytelnienia są tego rodzaju, istnieją też "
"sprzętowe schematy uwierzytelniania (takie jak używanie urządzeń "
"biometrycznych), które mają odpowiednie moduły, nadające się do "
"bezproblemowego podstawienia za standardowe modele uwierzytelniania - oto "
"elastyczność B<Linux-PAM>."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<password> - this group\\*(Aqs responsibility is the task of updating "
"authentication mechanisms\\&. Typically, such services are strongly coupled "
"to those of the B<auth> group\\&. Some authentication mechanisms lend "
"themselves well to being updated with such a function\\&. Standard UN*X "
"password-based access is the obvious example: please enter a replacement "
"password\\&."
msgstr ""
"B<password> - zadaniem tej grupy jest odświeżanie mechanizmów "
"uwierzytelniania. Zazwyczaj usługi takie są ściśle związane z tymi z "
"B<auth>.  Niektóre mechanizmy uwierzytelniania dobrze nadają się do "
"odświeżania tą funkcją. Oczywistym przykładem jest standardowy UN*X-owy "
"dostęp oparty o hasło: proszę wstawić hasło zamienne."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<session> - this group of tasks cover things that should be done prior to a "
"service being given and after it is withdrawn\\&. Such tasks include the "
"maintenance of audit trails and the mounting of the user\\*(Aqs home "
"directory\\&. The B<session> management group is important as it provides "
"both an opening and closing hook for modules to affect the services "
"available to a user\\&."
msgstr ""
"B<session> - zadania tej grupy obejmują rzeczy, które powinny być dokonane "
"przed daniem usługi oraz po jej wycofaniu. Zadania takie to m.in obsługa "
"śladów rewizyjnych i montowanie katalogu domowego użytkownika.  Grupa "
"obsługi sesji jest ważna, gdyż udostępnia zarówno hak otwierający, jak i "
"zamykający modułów."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "/etc/pam\\&.conf"
msgstr "/etc/pam\\&.conf"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "the configuration file"
msgstr "plik konfiguracyjny"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "/etc/pam\\&.d"
msgstr "/etc/pam\\&.d"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"the B<Linux-PAM> configuration directory\\&. Generally, if this directory is "
"present, the /etc/pam\\&.conf file is ignored\\&."
msgstr ""
"Katalog konfiguracji B<Linux-PAM>. Generalnie, jeśli katalog ten jest "
"obecny, to plik /etc/pam.conf jest ignorowany."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "/usr/lib/pam\\&.d"
msgstr "/usr/lib/pam\\&.d"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
#| msgid ""
#| "the B<Linux-PAM> configuration directory\\&. Generally, if this directory "
#| "is present, the /etc/pam\\&.conf file is ignored\\&."
msgid ""
"the B<Linux-PAM> vendor configuration directory\\&. Files in /etc/pam\\&.d "
"override files with the same name in this directory\\&."
msgstr ""
"Katalog konfiguracji B<Linux-PAM>. Generalnie, jeśli katalog ten jest "
"obecny, to plik /etc/pam.conf jest ignorowany."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "E<lt>vendordirE<gt>/pam\\&.d"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
#| msgid ""
#| "the B<Linux-PAM> configuration directory\\&. Generally, if this directory "
#| "is present, the /etc/pam\\&.conf file is ignored\\&."
msgid ""
"the B<Linux-PAM> vendor configuration directory\\&. Files in /etc/pam\\&.d "
"and /usr/lib/pam\\&.d override files with the same name in this "
"directory\\&. Only available if Linux-PAM was compiled with vendordir "
"enabled\\&."
msgstr ""
"Katalog konfiguracji B<Linux-PAM>. Generalnie, jeśli katalog ten jest "
"obecny, to plik /etc/pam.conf jest ignorowany."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Typically errors generated by the B<Linux-PAM> system of libraries, will be "
"written to B<syslog>(3)\\&."
msgstr ""
"Typowe błędy generowane przez system B<Linux-PAM> są zapisywane do "
"B<syslog>(3)."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"DCE-RFC 86\\&.0, October 1995\\&. Contains additional features, but remains "
"backwardly compatible with this RFC\\&."
msgstr ""
"DCE-RFC 86.0, Październik 1995. Zawiera dodatkowe właściwości, ale jest "
"wstecznie kompatybilny z tym RFC."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<pam>(3), B<pam_authenticate>(3), B<pam_sm_setcred>(3), B<pam_strerror>(3), "
"B<PAM>(7)"
msgstr ""
"B<pam>(3), B<pam_authenticate>(3), B<pam_sm_setcred>(3), B<pam_strerror>(3), "
"B<PAM>(7)"

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "01/08/2021"
msgid "09/03/2021"
msgstr "08.01.2021 r."
