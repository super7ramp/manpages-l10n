# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2022-06-16 17:37+0200\n"
"PO-Revision-Date: 2022-06-03 19:51+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "VORBISTAGEDIT"
msgstr "VORBISTAGEDIT"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "2006-11-17"
msgstr "17. november 2006"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "1.1.1"
msgstr "1.1.1"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "VORBIS-TOOLS"
msgstr "VORBIS-TOOLS"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "vorbistagedit - allows batch editing of vorbis comments with an editor"
msgstr ""
"vorbistagedit - tillader kørselsredigering af vorbis-kommentarer med et "
"redigeringsprogram"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<vorbistagedit> I<file1> B<[>I<\\|file2>B<\\ \\|[>I<\\|file3\\ ..."
">B<\\|]>I<\\|>B<]>"
msgstr ""
"B<vorbistagedit> I<fil1> B<[>I<\\|fil2>B<\\ \\|[>I<\\|fil3\\ ...>B<\\|]>I<\\|"
">B<]>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<vorbistagedit> B<[>\\|--versionB<|>-VB<|>-v\\|B<]>"
msgstr "B<vorbistagedit> B<[>\\|--versionB<|>-VB<|>-v\\|B<]>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<vorbistagedit> B<[>\\|--helpB<|>-h\\|B<]>"
msgstr "B<vorbistagedit> B<[>\\|--helpB<|>-h\\|B<]>"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<vorbistagedit> allows batch editing of vorbis comments with an editor.\\ "
"If more than one OGG Vorbis file is specified, B<vorbistagedit> opens a "
"unique editor for all files given. The supported file extensions for "
"B<vorbistagedit> are .oga, .ogg, .ogv, .ogx, and .spx."
msgstr ""
"B<vorbistagedit> tillader kørselsredigering af vorbis-kommentarer med et "
"redigeringsprogram.\\ Hvis mere end en OGG Vorbis-fil er angivet, så åbner "
"B<vorbistagedit> et unikt redigeringsprogram for alle angivne filer. De "
"understøttede filendelser for B<vorbistagedit> er .oga, .ogg, .ogv, .ogx og ."
"spx."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "TILVALG"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v, -V, --version >"
msgstr "B<-v, -V, --version >"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Show the version of B<vorbistagedit>."
msgstr "Vis versionsnummeret for B<vorbistagedit>."

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-h, --help >"
msgstr "B<-h, --help >"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Show a short help message."
msgstr "Vis en kort hjælpebesked."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "I<vorbiscomment>(1), I<ogginfo>(1)"
msgstr "I<vorbiscomment>(1), I<ogginfo>(1)"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MILJØ"

#. type: TP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<EDITOR>"
msgstr "B<EDITOR>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Defines the default editor.\\ If it's not defined, then I<sensible-"
"editor>(1)  is used."
msgstr ""
"Definerer standarden for redigeringsprogrammet.\\ Hvis den ikke er "
"defineret, så bruges I<sensible-editor>(1)."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<vorbistagedit> was written by Martin F. Krafft E<lt>vorbistagedit@pobox."
"madduck.netE<gt>."
msgstr ""
"B<vorbistagedit> blev skrevet af Martin F. Krafft E<lt>vorbistagedit@pobox."
"madduck.netE<gt>."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This manual page was written by Francois Wendling E<lt>frwendling@free."
"frE<gt> for the Debian project (but may be used by others)."
msgstr ""
"Denne manualside blev skrevet af Francois Wendling E<lt>frwendling@free."
"frE<gt> for Debianprojektet (men kan også bruges af andre)."
