# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jaakko Puurunen <jaakko.puurunen@iki.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 19:17+0200\n"
"PO-Revision-Date: 1998-04-10 11:01+0200\n"
"Last-Translator: Jaakko Puurunen <jaakko.puurunen@iki.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "UNEXPAND"
msgstr "UNEXPAND"

#. type: TH
#: archlinux
#, no-wrap
msgid "April 2022"
msgstr "Huhtikuuta 2022"

#. type: TH
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellukset"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "unexpand - convert spaces to tabs"
msgstr "unexpand - muunna välilyönnit sarkaimiksi"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<unexpand> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<unexpand> [I<\\,VALITSIN\\/>]... [I<\\,TIEDOSTO\\/>]..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Convert blanks in each FILE to tabs, writing to standard output."
msgstr ""
"Muunna kunkin TIEDOSTOn sisältämät välilyönnit sarkaimiksi, kirjoittaen "
"vakiotulosteeseen."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Jos TIEDOSTOa ei ole annettu, tai se on ”-”, luetaan vakiosyötettä."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Pitkien valitsinten pakolliset argumentit ovat pakollisia myös lyhyille."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "convert all blanks, instead of just initial blanks"
msgstr "muunna kaikki tyhjeet, ei vain rivien alussa olevia"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--first-only>"
msgstr "B<--first-only>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "convert only leading sequences of blanks (overrides B<-a>)"
msgstr "muunna vain rivien alussa olevat tyhjeet (kumoaa valitsimen B<-a>)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--tabs>=I<\\,N\\/>"
msgstr "B<-t>, B<--tabs>=I<\\,KOKO\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "have tabs N characters apart instead of 8 (enables B<-a>)"
msgstr "aseta sarkainkooksi N, ei 8 (ottaa käyttöön valitsimen B<-a>)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--tabs>=I<\\,LIST\\/>"
msgstr "B<-t>, B<--tabs>=I<\\,LUETTELO\\/>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "use comma separated list of tab positions The last specified position can "
#| "be prefixed with '/' to specify a tab size to use after the last "
#| "explicitly specified tab stop.  Also a prefix of '+' can be used to align "
#| "remaining tab stops relative to the last specified tab stop instead of "
#| "the first column"
msgid ""
"use comma separated list of tab positions.  The last specified position can "
"be prefixed with '/' to specify a tab size to use after the last explicitly "
"specified tab stop.  Also a prefix of '+' can be used to align remaining tab "
"stops relative to the last specified tab stop instead of the first column"
msgstr ""
"Käytä pilkuilla erotettua luetteloa sarkainten kohdista. Viimeinen "
"määritetty sijainti voidaan lisätä etuliitteenä '/' määrittää välilehden "
"koon, jota käytetään viimeisen eksplisiittisesti määritetty sarkainkoe.  "
"Myös etuliite \"+\" voidaan käyttää tasata jäljellä olevat sarkainkohdat "
"suhteessa viimeinen määritetty sarkainkoe ensimmäisen sarakkeen sijaan."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "näytä tämä ohje ja poistu"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "näytä versiotiedot ja poistu"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by David MacKenzie."
msgstr "Kirjoittanut David MacKenzie."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "VIRHEISTÄ ILMOITTAMINEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Ilmoita käännösvirheistä osoitteeseen E<lt>https://translationproject.org/"
"team/fi.htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "TEKIJÄNOIKEUDET"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Tämä on vapaa ohjelmisto; sitä saa vapaasti muuttaa ja levittää edelleen. "
"Siinä määrin kuin laki sallii, TAKUUTA EI OLE."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "expand(1)"
msgid "B<expand>(1)"
msgstr "B<expand>(1)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/unexpandE<gt>"
msgstr ""
"Koko dokumentaatio: E<lt>https://www.gnu.org/software/coreutils/unexpandE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"or available locally via: info \\(aq(coreutils) unexpand invocation\\(aq"
msgstr ""
"tai saatavilla paikallisesti: info \\(aq(coreutils) unexpand invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Syyskuuta 2020"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"use comma separated list of tab positions The last specified position can be "
"prefixed with '/' to specify a tab size to use after the last explicitly "
"specified tab stop.  Also a prefix of '+' can be used to align remaining tab "
"stops relative to the last specified tab stop instead of the first column"
msgstr ""
"Käytä pilkuilla erotettua luetteloa sarkainten kohdista. Viimeinen "
"määritetty sijainti voidaan lisätä etuliitteenä '/' määrittää välilehden "
"koon, jota käytetään viimeisen eksplisiittisesti määritetty sarkainkoe.  "
"Myös etuliite \"+\" voidaan käyttää tasata jäljellä olevat sarkainkohdat "
"suhteessa viimeinen määritetty sarkainkoe ensimmäisen sarakkeen sijaan."

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
msgid "expand(1)"
msgstr "B<expand>(1)"

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "Lokakuuta 2021"

#. type: TH
#: fedora-37 fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "August 2021"
msgid "August 2022"
msgstr "Elokuu 2021"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "Syyskuuta 2021"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "GNU coreutils 9.0"

#. type: Plain text
#: mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  Lisenssi GPLv3+: GNU "
"GPL versio 3 tai myöhempi E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
