# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gérard Delafond <gerard@delafond.org>
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2000.
# Sébastien Blanchet, 2002.
# Emmanuel Araman <Emmanuel@araman.org>, 2002.
# Éric Piel <eric.piel@tremplin-utc.net>, 2005.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Romain Doumenc <rd6137@gmail.com>, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011-2014.
msgid ""
msgstr ""
"Project-Id-Version: e2fsprogs\n"
"POT-Creation-Date: 2022-08-19 18:46+0200\n"
"PO-Revision-Date: 2022-05-17 17:19+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "E4DEFRAG"
msgstr "E4DEFRAG"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "May 2009"
msgstr "Mai 2009"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "e4defrag version 2.0"
msgstr "e4defrag version 2.0"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "e4defrag - online defragmenter for ext4 file system"
msgstr "e4defrag - Défragmenteur en ligne pour le système de fichiers ext4"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<e4defrag> [ B<-c> ] [ B<-v> ] I<target> \\&..."
msgstr "B<e4defrag> [ B<-c> ] [ B<-v> ] I<cible> \\&..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<e4defrag> reduces fragmentation of extent based file. The file targeted by "
"B<e4defrag> is created on ext4 file system made with \"-O extent\" option "
"(see B<mke2fs>(8)).  The targeted file gets more contiguous blocks and "
"improves the file access speed."
msgstr ""
"B<e4defrag> réduit la fragmentation du fichier basé sur les extents. Le "
"fichier ciblé par B<e4defrag> est présent sur les systèmes de fichiers ext4 "
"créés avec l'option B<-O extent> (consultez B<mke2fs>(8)). Le fichier ciblé "
"obtient ainsi plus de blocs contigus, ce qui améliore la vitesse d'accès à "
"ce fichier."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"I<target> is a regular file, a directory, or a device that is mounted as "
"ext4 file system.  If I<target> is a directory, B<e4defrag> reduces "
"fragmentation of all files in it. If I<target> is a device, B<e4defrag> gets "
"the mount point of it and reduces fragmentation of all files in this mount "
"point."
msgstr ""
"I<cible> est un fichier normal, un répertoire ou un périphérique monté ayant "
"un système de fichiers ext4. Si I<cible> est un répertoire, B<e4defrag> "
"réduit la fragmentation de tous les fichiers qu'il contient. Si I<cible> est "
"un périphérique, B<e4defrag> réduit la fragmentation de tous les fichiers "
"contenus dans son point de montage."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Get a current fragmentation count and an ideal fragmentation count, and "
"calculate fragmentation score based on them. By seeing this score, we can "
"determine whether we should execute B<e4defrag> to I<target>.  When used "
"with B<-v> option, the current fragmentation count and the ideal "
"fragmentation count are printed for each file."
msgstr ""
"Obtenir l'état de fragmentation actuel et l'état de fragmentation idéal, et "
"calculer à partir de ces informations un score de fragmentation. En fonction "
"de ce score, on peut déterminer s'il faut ou non exécuter B<e4defrag> sur "
"I<cible>. Lorsque l'option B<-v> est utilisée, les états de fragmentation "
"actuelle et idéale sont affichés pour chaque fichier."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Also this option outputs the average data size in one extent. If you see it, "
"you'll find the file has ideal extents or not. Note that the maximum extent "
"size is 131072KB in ext4 file system (if block size is 4KB)."
msgstr ""
"Cette option affiche aussi la taille moyenne des données par extent. Il est "
"possible de déterminer en la voyant si un fichier a des extents idéaux ou "
"non. Notez que la taille maximale d'un extent est 131 072 ko pour un système "
"de fichiers ext4 (si la taille d'un bloc est 4 ko)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "If this option is specified, I<target> is never defragmented."
msgstr "Si cette option est indiquée, I<cible> ne sera jamais défragmentée."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Print error messages and the fragmentation count before and after defrag for "
"each file."
msgstr ""
"Afficher les messages d'erreur et l'état de fragmentation avant et après la "
"défragmentation de chaque fichier."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<e4defrag> does not support swap file, files in lost+found directory, and "
"files allocated in indirect blocks. When I<target> is a device or a mount "
"point, B<e4defrag> doesn't defragment files in mount point of other device."
msgstr ""
"B<e4defrag> ne gère pas les fichiers d'échange (« swap files »), les "
"fichiers dans le répertoire lost+found et les fichiers alloués dans des "
"blocs indirects. Lorsque I<cible> est un périphérique ou un point de "
"montage, B<e4defrag> ne défragmente pas les fichiers d'autres points de "
"montage de périphériques."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"It is safe to run e4defrag on a file while it is actively in use by another "
"application.  Since the contents of file blocks are copied using the page "
"cache, this can result in a performance slowdown to both e4defrag and the "
"application due to contention over the system's memory and disk bandwidth."
msgstr ""
"Il n’y a aucun risque à exécuter B<e4defrag> sur un fichier alors qu’il est "
"activement utilisé par une autre application. Puisque le contenu des blocs "
"du fichier sont copiés en utilisant le cache de page, cela peut aboutir à "
"une baisse de performance pour B<e4defrag> et pour l’application à cause "
"d’un conflit d’accès à la mémoire du système et d’utilisation de bande "
"passante."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If the file system's free space is fragmented, or if there is insufficient "
"free space available, e4defrag may not be able to improve the file's "
"fragmentation."
msgstr ""
"Si l’espace libre du système est fragmenté ou s’il n’existe pas suffisamment "
"d’espace libre, B<e4defrag> pourrait ne pas pouvoir améliorer la "
"fragmentation du fichier."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Non-privileged users can execute B<e4defrag> to their own file, but the "
"score is not printed if B<-c> option is specified. Therefore, it is "
"desirable to be executed by root user."
msgstr ""
"Les utilisateurs sans droits particuliers peuvent exécuter B<e4defrag> sur "
"leurs propres fichiers, mais le score n'est pas affiché si l'option B<-c> "
"est indiquée. Il est donc préférable de l'utiliser avec les droits du "
"superutilisateur."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Written by Akira Fujita E<lt>a-fujita@rs.jp.nec.comE<gt> and Takashi Sato "
"E<lt>t-sato@yk.jp.nec.comE<gt>."
msgstr ""
"Écrit par Akira Fujita E<lt>I<a-fujita@rs.jp.nec.com>E<gt> et Takashi Sato "
"E<lt>I<t-sato@yk.jp.nec.com>E<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mke2fs>(8), B<mount>(8)."
msgstr "B<mke2fs>(8), B<mount>(8)."
