# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-08-19 18:58+0200\n"
"PO-Revision-Date: 2021-12-15 12:31+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MQ_GETATTR"
msgstr "MQ_GETATTR"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "mq_getattr, mq_setattr - get/set message queue attributes"
msgstr ""
"mq_getattr, mq_setattr - Lire et écrire les attributs d'une file de messages"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>mqueue.hE<gt>>\n"
msgstr "B<#include E<lt>mqueue.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int mq_getattr(mqd_t >I<mqdes>B<, struct mq_attr *>I<attr>B<);>\n"
"B<int mq_setattr(mqd_t >I<mqdes>B<, const struct mq_attr *restrict >I<newattr>B<,>\n"
"B<               struct mq_attr *restrict >I<oldattr>B<);>\n"
msgstr ""
"B<int mq_getattr(mqd_t >I<mqdes>B<, struct mq_attr *>I<attr>B<);>\n"
"B<int mq_setattr(mqd_t >I<mqdes>B<, const struct mq_attr *restrict >I<newattr>B<,>\n"
"B<               struct mq_attr *restrict >I<oldattr>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Link with I<-lrt>."
msgstr "Éditer les liens avec I<-lrt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<mq_getattr>()  and B<mq_setattr>()  respectively retrieve and modify "
"attributes of the message queue referred to by the message queue descriptor "
"I<mqdes>."
msgstr ""
"Les fonctions B<mq_getattr>() et B<mq_setattr>() extraient et modifient "
"respectivement les attributs de la file de messages référencée par le "
"descripteur de file de messages I<mqdes>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<mq_getattr>()  returns an I<mq_attr> structure in the buffer pointed by "
"I<attr>.  This structure is defined as:"
msgstr ""
"B<mq_getattr>() renvoie une structure I<mq_attr> dans le tampon pointé par "
"I<attr>. Cette structure est définie comme suit\\ :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct mq_attr {\n"
"    long mq_flags;       /* Flags: 0 or O_NONBLOCK */\n"
"    long mq_maxmsg;      /* Max. # of messages on queue */\n"
"    long mq_msgsize;     /* Max. message size (bytes) */\n"
"    long mq_curmsgs;     /* # of messages currently in queue */\n"
"};\n"
msgstr ""
"struct mq_attr {\n"
"    long mq_flags;       /* Drapeaux : 0 or O_NONBLOCK */\n"
"    long mq_maxmsg;      /* Max. # de messages dans la file */\n"
"    long mq_msgsize;     /* Max de la taille du message (octets) */\n"
"    long mq_curmsgs;     /* # de messages actuellement dans la file */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<mq_flags> field contains flags associated with the open message queue "
"description.  This field is initialized when the queue is created by "
"B<mq_open>(3).  The only flag that can appear in this field is B<O_NONBLOCK>."
msgstr ""
"Le champ I<mq_flags> contient des drapeaux associés à la description de la "
"file de messages ouverte. Ce champ est initialisé lorsque la file est créée "
"avec B<mq_open>(3). Le seul drapeau qui peut apparaître dans ce champ est "
"B<O_NONBLOCK>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<mq_maxmsg> and I<mq_msgsize> fields are set when the message queue is "
"created by B<mq_open>(3).  The I<mq_maxmsg> field is an upper limit on the "
"number of messages that may be placed on the queue using B<mq_send>(3).  The "
"I<mq_msgsize> field is an upper limit on the size of messages that may be "
"placed on the queue.  Both of these fields must have a value greater than "
"zero.  Two I</proc> files that place ceilings on the values for these fields "
"are described in B<mq_overview>(7)."
msgstr ""
"Les champs I<mq_maxmsg> et I<mq_msgsize> sont définis lorsque la file de "
"messages est créée avec B<mq_open>(3). Le champ I<mq_maxmsg> est le nombre "
"maximal de messages qui peuvent être placés dans la file avec B<mq_send>(3). "
"Le champ I<mq_msgsize> est la taille maximale des messages qui peuvent être "
"placés dans la file. Chacun de ces champs doit avoir une valeur supérieure à "
"zéro. Deux fichiers de I</proc> qui plafonnent ces valeurs pour ces champs "
"sont décrites dans B<mq_overview>(7)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<mq_curmsgs> field returns the number of messages currently held in the "
"queue."
msgstr ""
"Le champ I<mq_curmsgs> renvoie le nombre de messages actuellement dans la "
"file."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<mq_setattr>()  sets message queue attributes using information supplied in "
"the I<mq_attr> structure pointed to by I<newattr>.  The only attribute that "
"can be modified is the setting of the B<O_NONBLOCK> flag in I<mq_flags>.  "
"The other fields in I<newattr> are ignored.  If the I<oldattr> field is not "
"NULL, then the buffer that it points to is used to return an I<mq_attr> "
"structure that contains the same information that is returned by "
"B<mq_getattr>()."
msgstr ""
"B<mq_setattr>() définit les attributs de la file de messages avec les "
"informations fournies par la structure I<mq_attr> pointée par I<newattr>. Le "
"seul attribut qui peut être modifié est le drapeau B<O_NONBLOCK> de "
"I<mq_flags>. Les autres champs de I<newattr> sont ignorés. Si le champ "
"I<oldattr> est non NULL, alors le tampon qu'il pointe est utilisé pour "
"renvoyer une structure I<mq_attr> contenant les mêmes informations que la "
"fonction B<mq_getattr>() renverrait."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"On success B<mq_getattr>()  and B<mq_setattr>()  return 0; on error, -1 is "
"returned, with I<errno> set to indicate the error."
msgstr ""
"En cas de succès, B<mq_getattr>() et B<mq_setattr>() renvoient B<0>. En cas "
"d'erreur, elles renvoient B<-1> et définissent I<errno> en conséquence."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The message queue descriptor specified in I<mqdes> is invalid."
msgstr ""
"Le descripteur de file de messages spécifié dans I<mqdes> n'est pas valable."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "I<newattr-E<gt>mq_flags> contained set bits other than B<O_NONBLOCK>."
msgstr ""
"I<newattr-E<gt>mq_flags> contient des bits définis autres que B<O_NONBLOCK>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mq_getattr>(),\n"
"B<mq_setattr>()"
msgstr ""
"B<mq_getattr>(),\n"
"B<mq_setattr>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"On Linux, B<mq_getattr>()  and B<mq_setattr>()  are library functions "
"layered on top of the B<mq_getsetattr>(2)  system call."
msgstr ""
"Sous Linux, B<mq_getattr>() et B<mq_setattr>()  sont des fonctions de "
"bibliothèque au dessus de l'appel système B<mq_getsetattr>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The program below can be used to show the default I<mq_maxmsg> and "
"I<mq_msgsize> values that are assigned to a message queue that is created "
"with a call to B<mq_open>(3)  in which the I<attr> argument is NULL.  Here "
"is an example run of the program:"
msgstr ""
"Le programme ci-dessous peut être utilisé pour afficher les valeurs par "
"défaut de I<mq_maxmsg> et I<mq_msgsize> assignées à une file de messages qui "
"est créée avec un appel à B<mq_open>(3) dans lequel le paramètre est NULL. "
"Voici un exemple d'exécution du programme."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out /testq>\n"
"Maximum # of messages on queue:   10\n"
"Maximum message size:             8192\n"
msgstr ""
"$ B<./a.out /testq>\n"
"Maximum # of messages on queue:   10\n"
"Maximum message size:             8192\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Since Linux 3.5, the following I</proc> files (described in "
"B<mq_overview>(7))  can be used to control the defaults:"
msgstr ""
"Depuis Linux 3.5, les fichiers suivants de I</proc> (décrits dans) "
"B<mq_overview>(7)) peuvent être utilisés pour contrôler les valeurs par "
"défaut :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<uname -sr>\n"
"Linux 3.8.0\n"
"$ B<cat /proc/sys/fs/mqueue/msg_default>\n"
"10\n"
"$ B<cat /proc/sys/fs/mqueue/msgsize_default>\n"
"8192\n"
msgstr ""
"$ B<uname -sr>\n"
"Linux 3.8.0\n"
"$ B<cat /proc/sys/fs/mqueue/msg_default>\n"
"10\n"
"$ B<cat /proc/sys/fs/mqueue/msgsize_default>\n"
"8192\n"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>mqueue.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>mqueue.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"
msgstr ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    mqd_t mqd;\n"
"    struct mq_attr attr;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    mqd_t mqd;\n"
"    struct mq_attr attr;\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s mq-name\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s mq-name\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    mqd = mq_open(argv[1], O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, NULL);\n"
"    if (mqd == (mqd_t) -1)\n"
"        errExit(\"mq_open\");\n"
msgstr ""
"    mqd = mq_open(argv[1], O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, NULL);\n"
"    if (mqd == (mqd_t) -1)\n"
"        errExit(\"mq_open\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (mq_getattr(mqd, &attr) == -1)\n"
"        errExit(\"mq_getattr\");\n"
msgstr ""
"    if (mq_getattr(mqd, &attr) == -1)\n"
"        errExit(\"mq_getattr\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"Maximum # of messages on queue:   %ld\\en\", attr.mq_maxmsg);\n"
"    printf(\"Maximum message size:             %ld\\en\", attr.mq_msgsize);\n"
msgstr ""
"   printf(\"Nbre maximal de messages de la file : %ld\\en\", attr.mq_maxmsg);\n"
"   printf(\"Taille maximale de message :          %ld\\en\", attr.mq_msgsize);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (mq_unlink(argv[1]) == -1)\n"
"        errExit(\"mq_unlink\");\n"
msgstr ""
"    if (mq_unlink(argv[1]) == -1)\n"
"        errExit(\"mq_unlink\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<mq_close>(3), B<mq_notify>(3), B<mq_open>(3), B<mq_receive>(3), "
"B<mq_send>(3), B<mq_unlink>(3), B<mq_overview>(7)"
msgstr ""
"B<mq_close>(3), B<mq_notify>(3), B<mq_open>(3), B<mq_receive>(3), "
"B<mq_send>(3), B<mq_unlink>(3), B<mq_overview>(7)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 juin 2020"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "B<int mq_getattr(mqd_t >I<mqdes>B<, struct mq_attr *>I<attr>B<);>\n"
msgstr "B<int mq_getattr(mqd_t >I<mqdes>B<, struct mq_attr *>I<attr>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<int mq_setattr(mqd_t >I<mqdes>B<, const struct mq_attr *>I<newattr>B<,>\n"
"B<                 struct mq_attr *>I<oldattr>B<);>\n"
msgstr ""
"B<int mq_setattr(mqd_t >I<mqdes>B<, const struct mq_attr *>I<newattr>B<,>\n"
"B<                 struct mq_attr *>I<oldattr>B<);>\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: SH
#: opensuse-leap-15-4
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
