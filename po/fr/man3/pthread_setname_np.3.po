# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-08-19 19:03+0200\n"
"PO-Revision-Date: 2018-09-10 20:55+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "PTHREAD_SETNAME_NP"
msgstr "PTHREAD_SETNAME_NP"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-08-27"
msgstr "27 août 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "pthread_setname_np, pthread_getname_np - set/get the name of a thread"
msgstr ""
"pthread_setname_np, pthread_getname_np - Définir ou obtenir le nom d'un "
"thread"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
#| "B<#include E<lt>pthread.hE<gt>>\n"
#| "B<int pthread_setname_np(pthread_t >I<thread>B<, const char *>I<name>B<);>\n"
#| "B<int pthread_getname_np(pthread_t >I<thread>B<,>\n"
#| "B<                       char *>I<name>B<, size_t >I<len>B<);>\n"
msgid ""
"B<int pthread_setname_np(pthread_t >I<thread>B<, const char *>I<name>B<);>\n"
"B<int pthread_getname_np(pthread_t >I<thread>B<, char *>I<name>B<, size_t >I<len>B<);>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>pthread.hE<gt>>\n"
"B<int pthread_setname_np(pthread_t >I<thread>B<, const char *>I<name>B<);>\n"
"B<int pthread_getname_np(pthread_t >I<thread>B<,>\n"
"B<                       char *>I<name>B<, size_t >I<len>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Compile and link with I<-pthread>."
msgstr "Compiler et éditer les liens avec I<-pthreads>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"By default, all the threads created using B<pthread_create>()  inherit the "
"program name.  The B<pthread_setname_np>()  function can be used to set a "
"unique name for a thread, which can be useful for debugging multithreaded "
"applications.  The thread name is a meaningful C language string, whose "
"length is restricted to 16 characters, including the terminating null byte "
"(\\(aq\\e0\\(aq).  The I<thread> argument specifies the thread whose name is "
"to be changed; I<name> specifies the new name."
msgstr ""
"Par défaut, tous les threads créés par B<pthread_create>() héritent du nom "
"de programme. La fonction B<pthread_setname_np>() permet d'affecter un nom "
"propre à un thread, ce qui peut être utile pour déboguer une application "
"multithread. Le nom du thread est une chaîne de langage C significative, "
"dont la longueur ne peut dépasser 16 caractères, caractère nul final "
"(\\(aq\\e0\\(aq) inclus. L'argument I<thread> indique le thread dont le nom "
"doit être changé ; le nouveau nom est précisé dans I<name>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<pthread_getname_np>()  function can be used to retrieve the name of "
"the thread.  The I<thread> argument specifies the thread whose name is to be "
"retrieved.  The buffer I<name> is used to return the thread name; I<len> "
"specifies the number of bytes available in I<name>.  The buffer specified by "
"I<name> should be at least 16 characters in length.  The returned thread "
"name in the output buffer will be null terminated."
msgstr ""
"La fonction B<pthread_getname_np>() permet de récupérer le nom du thread. "
"L'argument I<thread> indique le thread dont le nom doit être récupéré. Le "
"tampon I<name> est utilisé pour renvoyer le nom du thread ; I<len> indique "
"le nombre d'octets disponibles dans I<name>. La longueur du tampon indiqué "
"dans I<name> doit être d'au moins 16 caractères. Le nom du thread renvoyé "
"dans le tampon de retour est terminé par caractère nul."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"On success, these functions return 0; on error, they return a nonzero error "
"number."
msgstr ""
"En cas de succès, ces fonctions renvoient B<0> ; en cas d'erreur, elles "
"renvoient un code d'erreur non nul."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<pthread_setname_np>()  function can fail with the following error:"
msgstr ""
"La fonction B<pthread_setname_np>() peut échouer avec l'erreur suivante :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The length of the string specified pointed to by I<name> exceeds the allowed "
"limit."
msgstr ""
"La longueur de la chaîne vers laquelle pointe I<name> dépasse la limite "
"autorisée."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<pthread_getname_np>()  function can fail with the following error:"
msgstr ""
"La fonction B<pthread_getname_np>() peut échouer avec l'erreur suivante :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The buffer specified by I<name> and I<len> is too small to hold the thread "
"name."
msgstr ""
"Le tampon indiqué par I<name> et I<len> a une taille insuffisante pour "
"contenir le nom du thread."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If either of these functions fails to open I</proc/self/task/[tid]/comm>, "
"then the call may fail with one of the errors described in B<open>(2)."
msgstr ""
"Si l'une de ces fonctions ne parvient pas à ouvrir I</proc/self/task/[tid]/"
"comm>, alors l'appel peut échouer en retournant l'une des erreurs décrites "
"dans B<open>(2)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "These functions first appeared in glibc in version 2.12."
msgstr "Ces fonctions ont été introduites dans la glibc dans sa version 2.12."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy, no-wrap
msgid ""
"B<pthread_setname_np>(),\n"
"B<pthread_getname_np>()"
msgstr "B<pthread_attr_getstack>(), B<pthread_attr_setstack>() :"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These functions are nonstandard GNU extensions; hence the suffix "
"\"_np\" (nonportable) in the names."
msgstr ""
"Ces fonctions sont des extensions GNU non standard ; d'où le suffixe "
"« _np » (non portable) dans leur nom."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<pthread_setname_np>()  internally writes to the thread-specific I<comm> "
"file under the I</proc> filesystem: I</proc/self/task/[tid]/comm>.  "
"B<pthread_getname_np>()  retrieves it from the same location."
msgstr ""
"B<pthread_setname_np>() écrit en interne dans le fichier I<comm> du thread "
"(dans le système de fichiers I</proc>) : I</proc/self/task/[tid]/comm>. "
"B<pthread_getname_np>() récupère le nom du thread à ce même endroit."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<pthread_setname_np>()  and "
"B<pthread_getname_np>()."
msgstr ""
"Le programme ci-dessous illustre l'utilisation de B<pthread_setname_np>() et "
"de B<pthread_getname_np>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The following shell session shows a sample run of the program:"
msgstr ""
"La session d'interpréteur suivant montre un échantillon d'exécution du "
"programme :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "$B< ./a.out>\n"
#| "Created a thread. Default name is: a.out\n"
#| "The thread name after setting it is THREADFOO.\n"
#| "B<^Z>                           # Suspend the program\n"
#| "[1]+  Stopped           ./a.out\n"
#| "$ B<ps H -C a.out -o 'pid tid cmd comm'>\n"
#| "  PID   TID CMD                         COMMAND\n"
#| " 5990  5990 ./a.out                     a.out\n"
#| " 5990  5991 ./a.out                     THREADFOO\n"
#| "$ B<cat /proc/5990/task/5990/comm>\n"
#| "a.out\n"
#| "$ B<cat /proc/5990/task/5991/comm>\n"
#| "THREADFOO\n"
msgid ""
"$B< ./a.out>\n"
"Created a thread. Default name is: a.out\n"
"The thread name after setting it is THREADFOO.\n"
"B<\\(haZ>                           # Suspend the program\n"
"[1]+  Stopped           ./a.out\n"
"$ B<ps H -C a.out -o \\(aqpid tid cmd comm\\(aq>\n"
"  PID   TID CMD                         COMMAND\n"
" 5990  5990 ./a.out                     a.out\n"
" 5990  5991 ./a.out                     THREADFOO\n"
"$ B<cat /proc/5990/task/5990/comm>\n"
"a.out\n"
"$ B<cat /proc/5990/task/5991/comm>\n"
"THREADFOO\n"
msgstr ""
"$B< ./a.out>\n"
"Thread créé. Le nom par défaut est : a.out\n"
"Le nom du thread après nommage est THREADFOO.\n"
"B<^Z>                           # Suspendre l'exécution du programme\n"
"[1]+  Stoppé            ./a.out\n"
"$ B<ps H -C a.out -o 'pid tid cmd comm'>\n"
"  PID   TID CMD                         COMMAND\n"
" 5990  5990 ./a.out                     a.out\n"
" 5990  5991 ./a.out                     THREADFOO\n"
"$ B<cat /proc/5990/task/5990/comm>\n"
"a.out\n"
"$ B<cat /proc/5990/task/5991/comm>\n"
"THREADFOO\n"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "#define NAMELEN 16\n"
msgstr "#define NAMELEN 16\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#define errExitEN(en, msg) \\e\n"
"                        do { errno = en; perror(msg); \\e\n"
"                             exit(EXIT_FAILURE); } while (0)\n"
msgstr ""
"#define errExitEN(en, msg) \\e\n"
"                        do { errno = en; perror(msg); \\e\n"
"                             exit(EXIT_FAILURE); } while (0)\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"static void *\n"
"threadfunc(void *parm)\n"
"{\n"
"    sleep(5);          // allow main program to set the thread name\n"
"    return NULL;\n"
"}\n"
msgstr ""
"static void *\n"
"threadfunc(void *parm)\n"
"{\n"
"    sleep(5);          // permet au programme principal de définir\n"
"                       // le nom du thread\n"
"    return NULL;\n"
"}\n"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char **argv)\n"
#| "{\n"
#| "    pthread_t thread;\n"
#| "    int rc;\n"
#| "    char thread_name[NAMELEN];\n"
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    pthread_t thread;\n"
"    int rc;\n"
"    char thread_name[NAMELEN];\n"
msgstr ""
"int\n"
"main(int argc, char **argv)\n"
"{\n"
"    pthread_t thread;\n"
"    int rc;\n"
"    char thread_name[NAMELEN];\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    rc = pthread_create(&thread, NULL, threadfunc, NULL);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_create\");\n"
msgstr ""
"    rc = pthread_create(&thread, NULL, threadfunc, NULL);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_create\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    rc = pthread_getname_np(thread, thread_name, NAMELEN);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_getname_np\");\n"
msgstr ""
"    rc = pthread_getname_np(thread, thread_name, NAMELEN);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_getname_np\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"Created a thread. Default name is: %s\\en\", thread_name);\n"
"    rc = pthread_setname_np(thread, (argc E<gt> 1) ? argv[1] : \"THREADFOO\");\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_setname_np\");\n"
msgstr ""
"    printf(\"Thread créé. Le nom par défaut est : %s\\en\", thread_name);\n"
"    rc = pthread_setname_np(thread, (argc E<gt> 1) ? argv[1] : \"THREADFOO\");\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_setname_np\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "    sleep(2);\n"
msgstr "    sleep(2);\n"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    rc = pthread_getname_np(thread, thread_name,\n"
#| "                            (argc E<gt> 2) ? atoi(argv[1]) : NAMELEN);\n"
#| "    if (rc != 0)\n"
#| "        errExitEN(rc, \"pthread_getname_np\");\n"
#| "    printf(\"The thread name after setting it is %s.\\en\", thread_name);\n"
msgid ""
"    rc = pthread_getname_np(thread, thread_name, NAMELEN);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_getname_np\");\n"
"    printf(\"The thread name after setting it is %s.\\en\", thread_name);\n"
msgstr ""
"    rc = pthread_getname_np(thread, thread_name,\n"
"                            (argc E<gt> 2) ? atoi(argv[1]) : NAMELEN);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_getname_np\");\n"
"    printf(\"Le nom du thread après nommage est %s.\\en\", thread_name);\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    rc = pthread_join(thread, NULL);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_join\");\n"
msgstr ""
"    rc = pthread_join(thread, NULL);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_join\");\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"Done\\en\");\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    printf(\"Terminé\\en\");\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<prctl>(2), B<pthread_create>(3), B<pthreads>(7)"
msgstr "B<prctl>(2), B<pthread_create>(3), B<pthreads>(7)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 novembre 2020"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>pthread.hE<gt>>\n"
"B<int pthread_setname_np(pthread_t >I<thread>B<, const char *>I<name>B<);>\n"
"B<int pthread_getname_np(pthread_t >I<thread>B<,>\n"
"B<                       char *>I<name>B<, size_t >I<len>B<);>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>pthread.hE<gt>>\n"
"B<int pthread_setname_np(pthread_t >I<thread>B<, const char *>I<name>B<);>\n"
"B<int pthread_getname_np(pthread_t >I<thread>B<,>\n"
"B<                       char *>I<name>B<, size_t >I<len>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"int\n"
"main(int argc, char **argv)\n"
"{\n"
"    pthread_t thread;\n"
"    int rc;\n"
"    char thread_name[NAMELEN];\n"
msgstr ""
"int\n"
"main(int argc, char **argv)\n"
"{\n"
"    pthread_t thread;\n"
"    int rc;\n"
"    char thread_name[NAMELEN];\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"    rc = pthread_getname_np(thread, thread_name,\n"
"                            (argc E<gt> 2) ? atoi(argv[1]) : NAMELEN);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_getname_np\");\n"
"    printf(\"The thread name after setting it is %s.\\en\", thread_name);\n"
msgstr ""
"    rc = pthread_getname_np(thread, thread_name,\n"
"                            (argc E<gt> 2) ? atoi(argv[1]) : NAMELEN);\n"
"    if (rc != 0)\n"
"        errExitEN(rc, \"pthread_getname_np\");\n"
"    printf(\"Le nom du thread après nommage est %s.\\en\", thread_name);\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: opensuse-leap-15-4
msgid "These functions are nonstandard GNU extensions."
msgstr "Ces fonctions sont des extensions GNU non standard."

#. type: SH
#: opensuse-leap-15-4
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: opensuse-leap-15-4
#, no-wrap
msgid ""
"$B< ./a.out>\n"
"Created a thread. Default name is: a.out\n"
"The thread name after setting it is THREADFOO.\n"
"B<^Z>                           # Suspend the program\n"
"[1]+  Stopped           ./a.out\n"
"$ B<ps H -C a.out -o 'pid tid cmd comm'>\n"
"  PID   TID CMD                         COMMAND\n"
" 5990  5990 ./a.out                     a.out\n"
" 5990  5991 ./a.out                     THREADFOO\n"
"$ B<cat /proc/5990/task/5990/comm>\n"
"a.out\n"
"$ B<cat /proc/5990/task/5991/comm>\n"
"THREADFOO\n"
msgstr ""
"$B< ./a.out>\n"
"Thread créé. Le nom par défaut est : a.out\n"
"Le nom du thread après nommage est THREADFOO.\n"
"B<^Z>                           # Suspendre l'exécution du programme\n"
"[1]+  Stoppé            ./a.out\n"
"$ B<ps H -C a.out -o 'pid tid cmd comm'>\n"
"  PID   TID CMD                         COMMAND\n"
" 5990  5990 ./a.out                     a.out\n"
" 5990  5991 ./a.out                     THREADFOO\n"
"$ B<cat /proc/5990/task/5990/comm>\n"
"a.out\n"
"$ B<cat /proc/5990/task/5991/comm>\n"
"THREADFOO\n"

#. type: Plain text
#: opensuse-leap-15-4
#, no-wrap
msgid ""
"#define errExitEN(en, msg) \\e\n"
"            do { errno = en; perror(msg); exit(EXIT_FAILURE); \\e\n"
"        } while (0)\n"
msgstr ""
"#define errExitEN(en, msg) \\e\n"
"            do { errno = en; perror(msg); exit(EXIT_FAILURE); \\e\n"
"        } while (0)\n"

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
